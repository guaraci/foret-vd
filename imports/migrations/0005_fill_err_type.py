from django.db import migrations


def fill_err_type(apps, schema_editor):
    ImpError = apps.get_model("imports", "ImpError")
    ImpError.objects.filter(message__contains='Le propriétaire').update(err_type='prop_missing')
    ImpError.objects.filter(message__contains='pas encore de protocole').update(err_type='no_prot')
    ImpError.objects.filter(message__contains='plus d\'un protocole').update(err_type='too_prot')
    ImpError.objects.filter(message__contains='lire ce code de tige').update(err_type='code_ill')
    ImpError.objects.filter(message__contains='Code d\'espèce').update(err_type='spec_unk')
    ImpError.objects.filter(message__contains='identifiant unique (uid)').update(err_type='uid_dbl')
    ImpError.objects.filter(message__contains='placette à double').update(err_type='plot_dbl')
    ImpError.objects.filter(message__contains='interpréter la ligne').update(err_type='line_ill')


class Migration(migrations.Migration):
    dependencies = [
        ('imports', '0004_imperror_err_type'),
    ]

    operations = [migrations.RunPython(fill_err_type)]
