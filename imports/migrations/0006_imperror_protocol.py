from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '__first__'),
        ('imports', '0005_fill_err_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='imperror',
            name='protocol',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='inventory.Protocol'),
        ),
    ]
