# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('imports', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fileimport',
            name='invent_year',
        ),
        migrations.RemoveField(
            model_name='fileimport',
            name='protocol',
        ),
        migrations.RemoveField(
            model_name='fileimport',
            name='spec_posE',
        ),
        migrations.RemoveField(
            model_name='fileimport',
            name='spec_posF',
        ),
        migrations.RemoveField(
            model_name='fileimport',
            name='vocab_interv',
        ),
        migrations.RemoveField(
            model_name='fileimport',
            name='vocab_pos16',
        ),
        migrations.RemoveField(
            model_name='fileimport',
            name='vocab_pos17',
        ),
        migrations.RemoveField(
            model_name='fileimport',
            name='vocab_posd6',
        ),
        migrations.RemoveField(
            model_name='fileimport',
            name='vocab_posd7',
        ),
        migrations.AlterField(
            model_name='imperror',
            name='fixed',
            field=models.CharField(default='', max_length=150, blank=True),
        ),
        migrations.AlterField(
            model_name='imperror',
            name='wrong',
            field=models.CharField(max_length=150),
        ),
    ]
