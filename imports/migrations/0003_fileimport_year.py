# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('imports', '0002_partial_rewrite'),
    ]

    operations = [
        migrations.AddField(
            model_name='fileimport',
            name='year',
            field=models.SmallIntegerField(default=2000, verbose_name="Ann\xe9e d'inventaire"),
            preserve_default=False,
        ),
    ]
