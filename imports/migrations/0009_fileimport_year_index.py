from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('imports', '0008_fileimport_fbase_check'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='fileimport',
            index=models.Index(fields=['year'], name='year_idx'),
        ),
    ]
