from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('imports', '0007_imperror_context'),
    ]

    operations = [
        migrations.AddField(
            model_name='fileimport',
            name='fbase_check',
            field=models.JSONField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='imperror',
            name='context',
            field=models.JSONField(blank=True, null=True),
        ),
    ]
