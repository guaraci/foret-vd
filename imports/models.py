import os
import re

from django.db import models
from django.db.models import Prefetch
from django.utils.html import strip_tags

from inventory.models import Protocol, Tree, TreeSpecies, Vocabulary
from inventory.models.db_views import AME_CE

# Format 1992 and before
main_line_regex_old = re.compile(
    r'A(?P<coordx>\d{4})(?P<coordy>\d{4})\s+B(?P<parc>\d{7}[0-9A-F]{,2})\s+'
    r'C(?P<inv>\d{6})\s+D(?P<desp>[0-9A-F][0-9A]\d[0-9A]\d?)\s+'
    r'0?(?P<gibi>[0-9A-F]{,9})(\s+(?P<trees>\*.*))?'
)
# Format after 1992
main_line_regex_new = re.compile(
    r'A(?P<coordx>\d{4})(?P<coordy>\d{4})\s+B(?P<parc>\d{7}[0-9A-F]{,2})\s+'
    r'C(?P<inv>\d{6})\s+D(?P<desp>[0-9A-F]{4}\d?)\s+'
    r'0(?P<gibi>[0-9A-F]{9})(\s+N(?P<id>\d{6}))?'
)


class FileImport(models.Model):
    ifile = models.FileField(upload_to='imported')
    year = models.SmallIntegerField("Année d'inventaire")
    imported = models.BooleanField(default=False)
    fbase_check = models.JSONField(blank=True, null=True)

    class Meta:
        indexes = [models.Index(name='year_idx', fields=['year'])]

    def __str__(self):
        return self.ifile.name

    def delete(self):
        # Also delete the import file.
        try:
            os.remove(self.ifile.path)
        except OSError:
            pass
        super().delete()

    @property
    def main_line_regex(self):
        return main_line_regex_old if self.year <= 1992 else main_line_regex_new

    @property
    def status(self):
        """Return either: 'imported', 'imported_with_errors', 'not_imported'"""
        if self.imported:
            return 'imported_with_errors' if self.pending_errors().count() else 'imported'
        else:
            return 'not_imported'

    def pending_errors(self):
        return self.errors.filter(fixed_ok=False)

    def check_fbases(self):
        identical = errors = 0
        different = []
        # Do not select_related 'imp_file' as existing fbase_check content can overflow memory
        imp_ops = self.plotobs_set.all().select_related(
            'plot', 'owner__typ', 'main_nature', 'interv_int', 'protocol',
        ).prefetch_related(
            Prefetch('tree_set', queryset=Tree.objects.select_related('spec'))
        )
        for obs in imp_ops:
            res = obs.check_fbase()
            if 'identical' in res:
                identical += 1
            elif isinstance(res, str) and res.startswith('Unable'):
                errors += 1
            else:
                different.append(res)
        self.fbase_check = {'identical': identical, 'different': different, 'errors': errors}
        self.save()

    @property
    def fbase_ok(self):
        return self.fbase_check and not self.fbase_check['different'] and not self.fbase_check.get('errors', 0)


class ImpError(models.Model):
    ERR_CHOICES = (
        ('code_ill', 'Code tige illisible'),
        ('spec_unk', 'Code d’espèce inconnu'),
        ('no_prot', 'Pas de protocole'),
        ('too_prot', 'Plusieurs protocoles possibles'),
        ('prop_missing', 'Propriétaire inexistant'),
        ('uid_dbl', 'Identifiant unique déjà existant'),
        ('plot_dbl', 'Placette à double'),
        ('line_ill', 'Ligne illisible'),
    )
    imp_file = models.ForeignKey(FileImport, related_name='errors', on_delete=models.CASCADE)
    no_line = models.SmallIntegerField()
    protocol = models.ForeignKey(Protocol, blank=True, null=True, on_delete=models.SET_NULL)
    raw_line = models.CharField(max_length=200)
    err_type = models.CharField("Type d’erreur", max_length=20, blank=True, choices=ERR_CHOICES)
    message = models.TextField()
    context = models.JSONField(blank=True, null=True)
    wrong = models.CharField(max_length=150)
    fixed = models.CharField(max_length=150, blank=True, default='')
    fixed_ok = models.BooleanField(default=False)

    class Meta:
        ordering = ('imp_file', 'no_line')

    @classmethod
    def add_err_span(cls, line, _from, _to):
        return line[0:_from] + '<span class="error">' + line[_from:_to] + '</span>' + line[_to:]

    def __str__(self):
        return "%d: %s" % (self.no_line, self.message)

    def get_raw_line(self):
        if self.err_type in ('no_prot', 'prop_missing'):
            return self.add_err_span(self.raw_line, 11, 15)
        elif self.err_type == 'spec_unk':
            try:
                loc = re.search(r'\*%s\d/' % self.wrong, self.raw_line).start()
                return self.add_err_span(self.raw_line, loc + 1, loc + 2)
            except Exception:
                pass
        return self.raw_line

    def ame_record(self):
        res_main = self.imp_file.main_line_regex.match(strip_tags(self.raw_line))
        if res_main is not None:
            uid = res_main.groupdict().get('id')
            if uid:
                try:
                    return AME_CE.objects.get(inv_GuidInv=uid)
                except AME_CE.DoesNotExist:
                    return None
