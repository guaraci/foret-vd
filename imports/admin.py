from django.contrib import admin

from . import models


class ErrorsInline(admin.TabularInline):
    model = models.ImpError

class FileImportAdmin(admin.ModelAdmin):
    search_fields = ('ifile',)
    inlines = [ErrorsInline]

admin.site.register(models.FileImport, FileImportAdmin)
admin.site.register(models.ImpError)
