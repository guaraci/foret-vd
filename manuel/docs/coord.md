# Coordonnées importantes

## Participants au projet

**Chef de projet du canton:**  
Nom:  
Téléphone:  
Courriel:

**Forestier de district:**  
Nom:  
Téléphone:  
Courriel:

**Forestier:**  
Nom:  
Téléphone:  
Courriel:

**Responsable de l'enregistrement:**  
Nom:  
Téléphone:  
Courriel:

## Numéros d'urgence

Urgence médicale: 144  
Rega: 1414  
Centre Hospitalier Universitaire de Vaud: 021 314 11 11  
Un autre hôpital ou médecin:
