# Préparation de l'inventaire forestier [par inv-vd.ch]

À partir de la page d'accueil du site, cliquer sur le bouton «Nouvel inventaire…».
Remplir les champs demandés du formulaire et cliquer sur Créer.

## Importation de points

Préparer ensuite un fichier Excel avec au minimum les colonnes CoordX, CoordY,
LvPropri, ame_SerieD, LvPropri_1, top_SerieD. L'ordre de ces colonnes n'est pas important.
Cliquer sur le bouton Importer des placettes et choisir le fichier préparé contenant
les points de base de l'inventaire.

Il est possible d'importer successivement plusieurs fichiers de points qui seront
chaque fois ajoutés à l'inventaire.

## Édition des points

À partir de là, une carte s'affiche sur la page de l'inventaire avec les points à inventorier
en blanc sur la carte. En cliquant sur un point, on peut voir les détails de ce point
apparaître sous la carte. Un bouton «Retirer ce point de l'inventaire» permet d'enlever
un point qui ne devrait pas être inventorié.
Le bouton «Modifier» permet de corriger les informations du point (propriétaire,
série, division), de définir quel opérateur sera chargé d'inventorier le point ou encore si
le point a été vérifié.

Quand un point a été vérifié, sa couleur passe du blanc au bleu.

## Ajouter un point manuellement

Il est aussi possible d'ajouter un nouveau point à inventorier en cliquant sur le bouton
«Ajouter un point sur la carte». Une icône ![symbole](https://www.inv-vd.ch/static/leaflet/images/marker-icon.png)
apparaît alors sur la carte. Il est possible de la déplacer en cliquant-glissant avec
la souris. Dès que vous relâchez la souris, le point se place automatiquement sur
le croisement de coordonnées le plus proche (arrondi à la centaine). Quand l'emplacement du
point vous semble correct, cliquez sur «Confirmer le nouveau point». Sinon cliquez sur
«Annuler le nouveau point». Il sera bien sûr nécessaire de modifier le nouveau
point pour donner les informations nécessaires du point.

## Modifier plusieurs points à la fois

Pour modifier les informations de plusieurs points en une seule opération, il est possible
de sélection plusieurs points en cliquant sur l'outil Lasso ![lasso](img/lasso.png) en haut à droite de la carte.
Entourez les points souhaités avec le lasso et au moment de relâcher la souris, les
points entourés sont sélectionnés (couleur mauve). Sous la carte, le nombre de
points sélectionnés apparaît, d'un même qu'un formulaire que vous pouvez remplir
pour attribuer la même valeur à tous les points sélectionnés. Les champs du formulaire que vous laissez vides ne seront pas touchés par la modification. Par exemple, si vous ne remplissez pas N° série, les numéros de série existants des points sélectionnés ne seront pas modifiés.

## Attributions aux mandataires/opérateurs

Les points à inventorier devront être attribués à un opérateur. Lorsque vous cliquez sur un point, vous pouvez voir si
un opérateur a déjà été attribué au point concerné. De plus, dès qu'un point possède un opérateur, son pourtour devient plus épais et coloré, chaque opérateur recevant automatiquement une couleur différente. Il est ainsi plus simple de visualiser les
points qui n'ont pas encore d'opérateur attribué.
Au moment de de connecter à l'application mobile pour l'inventaire, un opérateur ne verra apparaître que les points qui lui auront été attribués.

## Points déjà inventoriés

Quand les opérateurs se mettent à inventorier des placettes avec l'application mobile, la couleur du point sur la page de l'inventaire change et passe du bleu au vert. Il est donc possible de suivre au jour le jour la progression des inventaires.
En cliquant sur une placette verte (donc inventoriée), les informations saisies s'affichent sous la carte.
