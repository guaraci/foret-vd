# Introduction

## Avertissement

L'inventaire forestier par échantillonnage remplace avantageusement le
dénombrement pied par pied. Il exige toutefois beaucoup plus d'attention, de précision
et d'objectivité de la part de son exécutant.

Les valeurs enregistrées (mesurées, comptées ou estimées) sur une placette de 2, 3,
4, 5 ares + représentatives d'une population en général de ½ ha à 1 ha vont influer
en fonction de leur poids statistique sur diverses moyennes calculées d'une forêt
inventoriée. Toute erreur de mesure et d'affichage de la densité, de la pente et du
rayon fausse le calcul de la surface représentative de la placette et, du même coup,
compromet les résultats d'ensemble de l'inventaire. Le choix d'un rayon non
représentatif, l'absence d'identification des tiges limites, l'oubli ou la mesure à double
des tiges, les imprécisions dans la mesure des diamètres (position, hauteur,
orientation du compas, etc.) sont autant d'erreurs à proscrire d'un inventaire par
échantillonnage.

En bref, si l'opérateur observe strictement les règles suivantes:

 1. uniformité dans les mesures et les évaluations
 1. systématique de l'affichage
 1. rigueur dans le choix et la délimitation des échantillons (placette, tige, etc.)

Les avantages de l'inventaire Ipasmofix (rapidité, sensibilité, diversité des
informations) seront si évidents qu'ils compenseront largement l'handicap de l'erreur
statistique inhérente à l'échantillonnage.
