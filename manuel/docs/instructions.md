# Instructions techniques pour les appareils et l'application

## Tablette

- Tablette avec Android 10 ou supérieur (e.g. Samsung Galaxy Tab S4)
- Activation de Bluetooth (selon les instructions de la tablette (par exemple
Paramètres>Connexions>Bluetooth)

## Application

- Ouvrir le site <https://mobile.inv-vd.ch> avec le navigateur Chrome
- Sauvegarder la page web sur l'écran d'accueil (facultatif)
    - Liste (trois points) > «ajouter à l'écran d'accueil»
- Se connecter avec le nom d'utilisateur et le mot de passe
- Choisir l'inventaire de travail (liste déroulante)

## Navigation dans l'application

### Page 1 – Se connecter et sélectionner l'inventaire

![Page 1](img/mobile-page1.png)

1. Basculer entre la carte et la vue aérienne
1. Zoom
1. Accéder à ma position actuelle (GPS)
1. Activer/désactiver «les mesures»
1. Zoom sur l'inventaire
1. Placette
1. Nombre de placettes sauvegardées hors ligne
1. Synchronisation
1. Statut en ligne/hors ligne

### Page 2 – Sélectionner la placette

![Page 2](img/mobile-page2.png)

1. L'inventaire nouveau (rouge)
1. L'inventaire existant (noir)
1. Informations générales sur l’inventaire existant
1. Informations sur les arbres mesurés dans l’inventaire existant
1. Bouton pour retourner aux inventaires
1. Informations générales sur la placette

### Page 3 - Commencer l'inventaire

![Page 3](img/mobile-page3.png)

1. Bouton pour commencer l'inventaire.

### Page 4 – Les étapes de l'inventaire

![Page 4](img/mobile-page4.png)

1. Étape actuelle
1. Informations à vérifier/enregistrer
1. Champ de saisie verrouillé
1. Informations complémentaires sur le critère
1. Cocher et passer à l'étape suivante

### Page 5 – La dernière étape de l'inventaire

![Page 5](img/mobile-page5.png)

1. Étape actuelle
1. Ajouter un nouvel arbre
1. Liste des arbres ajoutés
1. Supprimer une entrée
1. Terminer et sauvegarder l'inventaire. Si une connexion Internet existe, les
données sont immédiatement stockées dans la base de données sur le serveur.
Sinon, les données sont temporairement stockées sur la tablette jusqu'à ce qu'une connexion au serveur puisse être établie.
1. Informations générales sur la placette
1. Bouton pour connecter une pince Bluetooth

## En ligne ou hors ligne

L'application permet de travailler en ligne et hors ligne. Pour la connexion internet, on peut utiliser soit une tablette avec une carte SIM, soit un point d'accès mobile à partir d'un smartphone.

Par précaution, il est recommandé de sauvegarder la zone de travail hors ligne, au cas où il n'y aurait pas ou peu de réseau à un point de test (placette).

Les points suivants doivent être respectés :

- Ne sélectionnez pas une zone de stockage hors ligne trop grande.
- Seules les données d'un inventaire peuvent être stockées hors ligne.

Procédure:

- Ouvrir l'application en suivant les instructions du [chapitre Application](#application).
- Naviquer vers les placettes qui sont préparées pour le mode hors ligne
- Appuyer sur le [bouton 8](#page-1-se-connecter-et-selectionner-linventaire) et confirmer «Voulez-vous enregistrer localement les données et cartes visibles?» avec OK

## Mode Bluetooth sur la pince

Ces instructions concernent la pince Bluetooth Haglöf MDII et le boîtier Data Terminal (Haglöf DPII).
Préalabelement charger les deux appareils conformément aux instructions.

- Allumer le boîtier DPII avec le bouton 3

**<p style="text-align: center;">
![DPII](img/dpii.png){: #dpii-img }
</p>**

- Vérifier si l'appareil est en «slave-mode»
    - naviguer avec les boutons (4) jusqu'à SET > System set. > Com Port, puis sélectionner Bluetooth>Slave>Pair

**<p style="text-align: center;">
![DPII System Set](img/dpii-systemset.png)
</p>**

- Sélectionner ![DPII btn](img/dpii-btn.png) pour connecter la pince à la tablette
- Le couplage de la pince et de la tablette **se fait via l'application**

## Couplage entre application et pince Bluetooth

Le couplage de la pince avec l'application se fait via l'application. Le couplage a lieu dans la dernière étape avant la [mesure des tiges à dénombrer](#page-5-la-derniere-etape-de-linventaire).

- Ouvrir l'application en suivant [les instructions](#application).
- Sélectionner une placette qui n'a pas encore été enregistrée (couleur bleue)
- Démarrer la saisie en cliquant sur l'année (en rouge), puis «Commencer» (voir [page 3](#page-3-commencer-linventaire))
- Remplir chaque étape selon les instructions
- Cocher et passer à l'étape suivante avec le bouton vert (voir [page 4](#page-4-les-etapes-de-linventaire))
- Dans la dernière étape «Confirmation finale», sélectionner le bouton «Connecter» pour connecter la pince
- Si l'appareil n'est pas en mode de couplage, vérifier encore les étapes du [chapitre Bluetooth](#mode-bluetooth-sur-la-pince)
- Sélectionner l'appareil DPIIXXXX et appuyer sur «Coupler»

**<p style="text-align: center;">
![DPII System Set](img/dpii-coupler.png){: #dpii-coupler }
</p>**

- Le message suivant apparaît sur la tablette «Une pince Bluetooth est actuellement connectée»
- Les pinces et l'application sont couplées et prêtes à mesurer des tiges à dénombrer

## Mesurer les arbres

Une fois la pince connectée à la tablette, le terminal de données est directement en mode d'entrée.

- Utilisez les boutons 4 (gauche, droite) du terminal de données pour sélectionner les espèces d'arbres appropriées.
- Placez la pince sur l'arbre selon les instructions et

![DPII](img/dpii.png){: #dpii-img align=right }

**Variante 1**: le terminal de données est attaché à la pince avec le périphérique approprié.

- Utilisez le bouton d'entrée rouge sur la pince pour confirmer les espèces d'arbres et le diamètre. Le résultat est visible directement sur la tablette après un bip sonore.

**Variante 2 (non recommandée)**: le terminal de données n'est pas attaché à la pince, mais est porté (par exemple) au poignet.

- Utilisez le bouton 3 pour confirmer votre saisie. Le résultat est visible directement sur la tablette.


## GPS externe (facultatif)

Il est possible d'intégrer un GPS externe. Cela permet de déterminer la position de la placette de manière beaucoup plus fiable. En raison d'une plus grande précision de positionnement, l'inventaire peut être mieux combiné avec d'autres données disponibles à l'échelle de la zone, comme les données Lidar ou Sentinel.
