from django.db import models
from django.utils.translation import gettext_lazy as _


class Document(models.Model):
    doc = models.FileField(upload_to='documents', verbose_name=_("File"))
    title = models.CharField(max_length=255, verbose_name=_("Title"))
    date = models.DateField(verbose_name=_("Date"))
    weight = models.SmallIntegerField(default=0)

    @property
    def fformat(self):
        return self.doc.path.rsplit('.')[-1]
