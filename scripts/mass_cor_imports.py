import argparse
import os
import sys
from pathlib import Path

import django

sys.path.append(str(Path(__file__).resolve().parent.parent))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "common.settings")
django.setup()

parser = argparse.ArgumentParser()
parser.add_argument("--year", type=int)
parser.add_argument("--file")
parser.add_argument('--reimport', action='store_true')
args = parser.parse_args()

from django.core.exceptions import ValidationError
from django.core.files import File
from imports.models import FileImport
from imports.imports import Importer

base_dir = Path("/home/claude/FichiersCOR")
#base_dir = Path("/home/claude/Bureau/Temp/Guaraci/FichiersCOR")
 
for dossieryear in sorted(base_dir.iterdir()):
    year = int(dossieryear.name)
    if args.year and args.year != year:
        continue
    for nom_fichier in sorted(dossieryear.iterdir()):
        if args.file and args.file != nom_fichier.name:
            continue
        print(f"Trying import file {nom_fichier.name}")
        if args.reimport:
            try:
                imp_file = FileImport.objects.get(ifile=f'imported/{nom_fichier.name}')
            except FileImport.DoesNotExist:
                print(f"Unable to find {nom_fichier.name} to reimport.")
                continue
        else:
            with nom_fichier.open('r') as fh:
                try:
                    Importer.check_validity(fh, check_dup_name=True)
                except ValidationError as err:
                    print(err)
                    continue
                imp_file = FileImport(ifile=File(fh, name=nom_fichier.name), year=year)
                imp_file.save()
        importer = Importer(imp_file)
        try:
            num_imported = importer.do_import(continue_on_error=True)
        except Exception:
            imp_file.delete()
            raise
        print(f"Imported {num_imported} lines from {nom_fichier.name}, {imp_file.errors.count()} errors")
