var CACHE_NAME = 'vdinv-v1',
    debug = false,
    // No need to cache much at install time considering our "get-from-cache-or-fetch-and-cache" strategy.
    urlsToCache = [
      '/static/img/locked.svg',
      '/static/img/unlocked.svg',
      '/static/img/online.svg',
      '/static/img/offline.svg',
      '/static/img/plan.png',
      '/static/img/satellite.png',
      '/static/img/back.svg',
      '/static/img/okcontinue.svg',
      '/static/img/help.svg',
      '/static/img/cancel.svg',
      '/static/img/newtree.svg',
      '/static/img/tree.svg',
      '/static/img/tree-dead.svg',
      '/static/img/tree-special.svg',
      '/static/img/tree-todo.svg'
    ];

self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME).then(function (cache) {
            return cache.addAll(urlsToCache);
        })
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(async function() {
        const cache = await caches.open(CACHE_NAME);
        const cachedResponse = await cache.match(event.request);
        let networkResponse = null;

        //event.waitUntil(async function() {
        try {
            networkResponse = await fetch(event.request);
        } catch(err) {
            return cachedResponse;
        }
        if (!networkResponse ||networkResponse.status !== 200 || networkResponse.type !== 'basic') {
            return networkResponse;
        }
        if (event.request.url.indexOf("/login/") < 0 && event.request.method == 'GET') {
            console.log("Caching " + event.request.url);
            await cache.put(event.request, networkResponse.clone());
        }
        //}());
        return networkResponse || cachedResponse;
    }());
});
