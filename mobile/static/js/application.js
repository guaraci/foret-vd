'use strict';
import * as Utils from './modules/utils.js';
import { Map } from './modules/map.js';
import { Caliper } from './modules/caliper.js';
import { FormSet } from './modules/forms.js';
import { MobileApp } from './modules/app.js';

/*
 * Map class
 */
class VDMap extends Map {
    static CENTER_INITIAL = [46.57, 6.65];
    static ZOOM_INITIAL = 17;
    static ZOOM_PLOT = 27;

    setupLayers() {
        this.gridLayer = new L.MetricGrid({
            proj4ProjDef: Utils.epsg2056String,
            bounds: [[2485000, 1075000] , [2830000, 1300000]],
            color: '#5272E5'
        });
        this.orthoLayer = L.tileLayer.swiss({layer: 'ch.swisstopo.swissimage'});
        this.grundLayer = L.tileLayer.swiss({layer: 'ch.swisstopo.pixelkarte-farbe', useCache: true});
        /*this.grundLayer = L.tileLayer.swiss({
            url: 'https://ows.asitvd.ch/wmts/1.0.0/{layer}/default/{timestamp}/0/2056/{z}/{y}/{x}.{format}',
            format: 'png',
            layer: 'asitvd.fond_couleur', # or 'asitvd.fond_cadastral',
            timestamp: 'default',
            attribution: '<a href="https://www.asitvd.ch/">ASIT VD</a>'
        });*/
        this.gridLayer = new L.MetricGrid({
            proj4ProjDef: Utils.epsg2056String,
            bounds: [[2485000, 1075000] , [2830000, 1300000]],
            color: '#5272E5'
        });
        this.baseLayers = [this.grundLayer, this.gridLayer];
        L.control.polylineMeasure({
            showBearings: true,
            tooltipTextFinish: 'Cliquer pour <b>terminer la ligne</b><br>',
            tooltipTextDelete: 'Appuyer sur SHIFT et cliquer pour <b>supprimer le point</b>',
            tooltipTextMove: 'Cliquer et glisser pour <b>déplacer le point</b><br>',
            tooltipTextResume: '<br>Appuyer sur CTRL et cliquer pour <b>continuer la ligne</b>',
            tooltipTextAdd: 'Appuyer sur CTRL et cliquer pour <b>ajouter un point</b>',
            measureControlTitleOn: 'Activer les mesures',
            measureControlTitleOff: 'Désactiver les mesures',
            clearControlTitle: 'Effacer les mesures',
            angleUnit: 'gradians'
        }).addTo(this.map);
    }

    showInventory(inventory, fitMap) {
        // we could display other inventory data here.
        return;
    }
}

class VDFormSet extends FormSet {
    constructor(app) {
        super(app);
        this.valueZeroDisableFields = ['id_gibi_epicea', 'id_gibi_conifer', 'id_gibi_leaved', 'id_unnumbered']
    }

    valueZeroDisable(fieldId) {
        // When choosing 0 for such dfields, the following percentage fields must be 0.
        const select = document.getElementById(fieldId),
              opt = select.options[select.selectedIndex];
        const disable = (opt && opt.value == '0'),
              targets = fieldId == 'id_unnumbered' ? ['id_conifer'] : [fieldId + '_f', fieldId + '_a'];
        targets.forEach(targetId => {
            let targetField = document.getElementById(targetId);
            if (disable) targetField.value = 0;
            targetField.disabled = disable;
        });
    }

    initHandlers() {
        this.valueZeroDisableFields.forEach(fieldId => {
            document.getElementById(fieldId).addEventListener(
                'change', this.valueZeroDisable.bind(this, fieldId)
            );
        });
        // Show or hide remarks if "not ok" radio is chosen at first step
        [document.querySelector('#verifnotok'), document.querySelector('#verifok')].forEach(radio => {
            radio.addEventListener('change', (ev) => {
                if (ev.target.value == 'notok')
                    document.querySelector('#step1-remarks').classList.remove('hidden');
                else document.querySelector('#step1-remarks').classList.add('hidden');
            });
        });
        super.initHandlers()
    }

    hideAll() {
        super.hideAll();
        document.getElementById('caliper-disconnected').style.display = 'none';
        document.getElementById('caliper-connected').style.display = 'none';
    }

    initForms(obs) {
        super.initForms(obs);
        Utils.getRadioValue('verif_step1') == 'notok' ? document.querySelector('#step1-remarks').classList.remove('hidden') : document.querySelector('#step1-remarks').classList.add('hidden');
        this.valueZeroDisableFields.forEach(fieldId => { this.valueZeroDisable(fieldId); });
    }

    initForm(form, obs, isNew, properties) {
        if (isNew) {
            if (form.id == 'step2' && form.elements.radius.value == '') form.elements.radius.value = 11;
            if (form.id == 'step2' && form.elements.main_perc.value == '') form.elements.main_perc.value = 100;
            if (form.id != 'step1') return;
        }
        super.initForm(form, obs, isNew, properties);
    }

    showStep(step) {
        super.showStep(step);
        if (step.startsWith('tree') || step == this.FINAL_STEP) {
            this.app.caliper.showStatus();
        }
    }
}

function speciesReceived(specCode, specChars) {
    // Start a new tree form
    window.app.formSet.showStep('tree-new')

    // Set species to the tree form
    let specOpt = document.querySelector(`[data-abrev='${specChars}']`);
    document.getElementById('id_spec').value = specOpt.value;
}

function diameterReceived(mm) {
    // Set diameter to the tree form (rounded in cm)
    document.getElementById('id_diameter').value = Math.round(mm / 10);

    // End the tree form.
    let treeForm = document.querySelector('#steptree');
    window.app.formSet.submitForm(null, treeForm);
    Utils.beep(100, 700, 250);
}

class VDApp extends MobileApp {
    constructor() {
        super();
        this.mapClass = VDMap;
        this.formSet = new VDFormSet(this);
        this.caliper = new Caliper(speciesReceived, diameterReceived);
    }

    initMap(mapOptions) {
        super.initMap({crs: L.CRS.EPSG2056}); //, maxBounds: L.TileLayer.Swiss.latLngBounds});
    }

    prepareInventory(inventory) {
        // Fill owner select with inventory.properties.owners.
        const ownerSelect = document.getElementById('id_owner');
        for(var i = ownerSelect.options.length - 1 ; i >= 0 ; i--) { ownerSelect.remove(i); }
        for(var i = 0; i < inventory.properties.owners.length; i++) {
            var opt = document.createElement("option");
            opt.value = inventory.properties.owners[i][0];
            opt.innerHTML = inventory.properties.owners[i][1];
            ownerSelect.appendChild(opt);
        }
        // Hide/unhide custom vocabulary selects
        const voc_trs = document.querySelectorAll('tr.voc_field');
        for (var i = 0; i < voc_trs.length; i++) {
            voc_trs[i].style.display = 'none';
        }
        for (var i = 0; i < inventory.properties.vocab_ids.length; i++) {
            var voc_tr = document.getElementById('tr_voc_' + inventory.properties.vocab_ids[i]);
            if (voc_tr) { voc_tr.style.display = 'table-row'}
        }
    }
}

window.app = new VDApp();

document.addEventListener("DOMContentLoaded", (ev) => {
    window.app.init();
});
