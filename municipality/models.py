from django.contrib.gis.db import models


class Arrondissement(models.Model):
    no = models.PositiveSmallIntegerField("Numéro")
    nom = models.CharField(max_length=50)
    geom = models.MultiPolygonField(srid=2056)

    def __str__(self):
        return self.nom

    class Meta:
        db_table = 'arrondissements'


class Municipality(models.Model):
    CANTON_VD = 22
    CANTON_CHOICES = (
        (2, 'BE'),
        (10, 'FR'),
        (CANTON_VD, 'VD'),
        (23, 'VS'),
        (24, 'NE'),
        (25, 'GE'),
        (9999, 'Autre'),
    )
    name = models.CharField("Nom", max_length=32, db_column='lad_nommin')
    canton = models.IntegerField(choices=CANTON_CHOICES, db_column='lad_nocant')
    no_ch = models.IntegerField("Numéro CH", db_column='lad_nofedc', blank=True, null=True)
    lad_nocanc = models.IntegerField("Numéro cantonal")
    lad_idsffn = models.IntegerField(blank=True, null=True)
    lad_nocand = models.IntegerField(blank=True, null=True)
    lad_nofedd = models.IntegerField(blank=True, null=True)
    geom = models.MultiPolygonField(srid=2056, blank=True, null=True)
    # VD Fields
    district_07 = models.CharField("District jusqu’en 2007", max_length=15, blank=True)
    district = models.CharField("District", max_length=25, blank=True)
    region_ofs = models.CharField("Région OFS + STG", max_length=8, blank=True)
    region_06 = models.CharField("Région depuis 2006", max_length=8, blank=True)
    arrond = models.ForeignKey(
        Arrondissement, on_delete=models.SET_NULL, verbose_name="Arrondissement", blank=True, null=True
    )
    triage = models.PositiveSmallIntegerField("Triage", blank=True, null=True)
    fusion_dans = models.ForeignKey('self', blank=True, null=True, on_delete=models.SET_NULL)
    fusion_date = models.DateField("Date de fusion", blank=True, null=True)

    class Meta:
        db_table = 'municipality'
        verbose_name = 'Commune'
        ordering = ('name',)

    def __str__(self):
        return self.name

    @classmethod
    def get_from_point(self, point):
        try:
            return Municipality.objects.get(geom__contains=point)
        except Municipality.DoesNotExist:
            return None
        except Municipality.MultipleObjectsReturned:
            # If only one of them is on VD, take it
            municips = Municipality.objects.filter(
                geom__contains=point, canton=Municipality.CANTON_VD
            )
            if len(municips) > 1:
                # Exclure les communes fusionnées
                municips = [m for m in municips if m.fusion_dans_id is None]
            if len(municips) == 1:
                return municips[0]
            else:
                raise Exception(
                    "Il existe plus d'une commune correspondant au point %s (%s)." % (
                        point.coords, ", ".join(str(m) for m in municips)
                ))
