from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin

from .models import Arrondissement, Municipality


@admin.register(Arrondissement)
class ArrondissementAdmin(LeafletGeoAdmin):
    list_display = ('nom', 'no')
    ordering = ('no',)


@admin.register(Municipality)
class MunicipalityAdmin(LeafletGeoAdmin):
    list_display = ('name', 'canton', 'lad_nocanc', 'no_ch', 'arrond', 'triage')
    list_filter = ('canton',)
    search_fields = ('name', 'lad_nocanc', 'no_ch')
    ordering = ('name',)
    exclude = ('lad_nomm_1', 'lad_nomm_2')
