from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('municipality', '0004_arrondissement'),
    ]

    operations = [
        migrations.RenameField(
            model_name='municipality',
            old_name='arrond',
            new_name='arrond_no',
        ),
        migrations.AddField(
            model_name='municipality',
            name='arrond',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='municipality.arrondissement', verbose_name='Arrondissement'),
        ),
    ]
