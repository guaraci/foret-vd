from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('municipality', '0006_migrate_arrond'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='municipality',
            name='arrond_no',
        ),
    ]
