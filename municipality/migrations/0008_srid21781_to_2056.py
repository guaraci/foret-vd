from django.contrib.gis.db.models.fields import MultiPolygonField
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('municipality', '0007_remove_municipality_arrond_no'),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(
            database_operations=[
                migrations.RunSQL("""
        ALTER TABLE municipality
         ALTER COLUMN geom TYPE geometry(MultiPolygon,2056)
          USING ST_Transform(geom,2056);"""),
            ],
            state_operations=[
                migrations.AlterField(
                    model_name='municipality',
                    name='geom',
                    field=MultiPolygonField(blank=True, null=True, srid=2056),
                ),
            ]
        )
    ]
