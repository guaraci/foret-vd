from django.db import migrations


def migrate_arronds(apps, schema_editor):
    Municipality = apps.get_model("municipality", "Municipality")
    Arrondissement = apps.get_model("municipality", "Arrondissement")
    for mun in Municipality.objects.filter(arrond_no__isnull=False):
        mun.arrond = Arrondissement.objects.get(no=mun.arrond_no)
        mun.save()


class Migration(migrations.Migration):

    dependencies = [
        ('municipality', '0005_municipality_arrond'),
    ]

    operations = [
        migrations.RunPython(migrate_arronds),
    ]
