import inspect
import re
import subprocess

from django.apps import apps
from django.conf import settings
from django.db import connection
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django.views.generic import TemplateView

'''We count on following available settings:
    DBDOCS_MAIN_APP_NAME = ''
    DBDOCS_LOOKUP_TABLE_WHITELIST = []
    DBDOCS_MAIN_TABLE_WHITELIST = []
'''


class DBView:
    """Introspection class for a database view."""
    def __init__(self, name=None, oid=None):
        if not name and not oid:
            raise ValueError("You must provide either name or oid")
        self._name = name
        self._oid = oid
        self._relkind = None

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __lt__(self, other):
         return self.name.lower() < other.name.lower()

    @property
    def oid(self):
        if self._oid:
            return int(self._oid)
        with connection.cursor() as cursor:
            cursor.execute("SELECT oid, relkind FROM pg_class WHERE relname=%s", [self._name])
            try:
                self._oid, self._relkind = cursor.fetchone()
            except TypeError:
                pass
        return self._oid

    @property
    def name(self):
        if self._name:
            return self._name
        with connection.cursor() as cursor:
            cursor.execute("SELECT relname, relkind FROM pg_class WHERE oid=%s", [self._oid])
            try:
                self._name, self._relkind = cursor.fetchone()
            except TypeError:
                pass
        return self._name

    @property
    def is_view(self):
        if self._relkind is None:
            self.name, self.oid  # Force db query
        return self._relkind in ('v', 'm')

    @cached_property
    def definition(self):
        with connection.cursor() as cursor:
            if self.is_view:
                cursor.execute("SELECT pg_get_viewdef(%s, true)", [self.oid])
                try:
                    definition = cursor.fetchone()[0]
                except TypeError:
                    return None
            else:
                try:
                    dump = subprocess.check_output(
                        "pg_dump -U %s -t 'public.%s' --schema-only --no-acl %s" % (
                            settings.DATABASES['default']['USER'], self.name,
                            settings.DATABASES['default']['NAME']
                        ),
                        env={'PGPASSWORD': settings.DATABASES['default'].get('PASSWORD', '')},
                        shell=True
                    )
                except subprocess.CalledProcessError as err:
                    return "Error retrieving table structure (%s)." % err
                # Grab the CREATE TABLE part
                capture = False
                definition = ''
                comments = self.field_comments()
                for line in dump.decode('utf-8').splitlines():
                    if 'CREATE TABLE' in line:
                        capture = True
                    if capture:
                        first_word = line.strip().split()[0]
                        comment = ''
                        if first_word in comments and comments[first_word] is not None:
                            comment = '    -- %s\n' % comments[first_word]
                        definition += comment + line + "\n"
                        if line == ");":
                            break
        return definition

    @cached_property
    def comment(self):
        with connection.cursor() as cursor:
            cursor.execute("SELECT obj_description(%s, 'pg_class')", [self.oid])
            return cursor.fetchone()[0]

    def field_comments(self):
        query = """SELECT
            cols.column_name,
            (
                SELECT
                    pg_catalog.col_description(c.oid, cols.ordinal_position::int)
                FROM
                    pg_catalog.pg_class c
                WHERE
                    c.oid = (SELECT ('"' || cols.table_name || '"')::regclass::oid)
                    AND c.relname = cols.table_name
            ) AS column_comment
        FROM
            information_schema.columns cols
        WHERE
            cols.table_catalog = %s AND cols.table_name = %s AND cols.table_schema = 'public';
        """
        with connection.cursor() as cursor:
            cursor.execute(query, (settings.DATABASES['default']['NAME'], self.name))
            return {line[0]: line[1] for line in cursor.fetchall()}

    def sub_views(self, recurse=False, only_views=False):
        """Return a list of dependent views or tables."""
        if not self.is_view:
            return []
        matches = re.findall(r'(?:FROM|JOIN)\s(\w+)', self.definition)
        sub_views = [DBView(name=view_name) for view_name in set(matches)]
        if recurse:
            [sub_views.extend(dbv.sub_views(recurse=True)) for dbv in sub_views]
        if only_views:
            return [sb for sb in sub_views if sb.is_view]
        return sub_views


class ViewList(TemplateView):
    template_name = "dbdocs/view_list.html"

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        # Get views from introspecting db_views
        with connection.cursor() as curs:
            db_view_list = [
                DBView(ti.name) for ti in connection.introspection.get_table_list(curs)
                if ti.type == 'v'
            ]
        for db_view in db_view_list[:]:
            db_view_list.extend(db_view.sub_views(recurse=True, only_views=True))
        context['dbviews'] = sorted(set(db_view_list))
        return context


class ViewDefinition(TemplateView):
    template_name = "dbdocs/view_def.html"

    def get(self, request, *args, **kwargs):
        self.db_view = DBView(oid=int(self.kwargs['oid']))
        if not self.db_view.is_view:
            # Try as a table object
            return HttpResponseRedirect(reverse('table-def', args=[self.kwargs['oid']]))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        # Find other views/tables referenced in the definition
        definition = self.db_view.definition
        for db_view in self.db_view.sub_views():
            definition = re.sub(
                r'(FROM|JOIN) %s ' % db_view.name,
                r'\1 <a href="%s">%s</a> ' % (reverse('view-def', args=[db_view.oid]), db_view.name),
                definition
            )
        func_matches = re.findall(r'(ksp_[a-z_]*)\(', definition)
        for func_name in set(func_matches):
            definition = definition.replace(
                func_name, '<a href="%s">%s</a>' % (
                    reverse('func-def', args=[func_name]), func_name
                )
            )
        context.update({
            'obj_type': "View",
            'name': self.db_view.name,
            'definition': definition,
            'comment': self.db_view.comment,
        })
        return context


class TableMixin:
    @property
    def lookup_model_whitelist(self):
        other_lookups = settings.DBDOCS_LOOKUP_TABLE_WHITELIST
        return [
            mod for mod in apps.get_app_config(settings.DBDOCS_MAIN_APP_NAME).get_models()
            if mod._meta.db_table.startswith('lt_') or mod._meta.db_table in other_lookups
        ]

    @property
    def main_model_whitelist(self):
        main_table_names = settings.DBDOCS_MAIN_TABLE_WHITELIST
        return [
            mod for mod in apps.get_app_config(settings.DBDOCS_MAIN_APP_NAME).get_models()
            if mod._meta.db_table in main_table_names
        ]

    def decorate_tabledef(self, table):
        '''Link FKs to their tables'''
        definition = table.definition
        with connection.cursor() as cursor:
            for field, ext_table, ext_field in connection.introspection.get_key_columns(cursor, table.name):
                definition = definition.replace(
                    ' %s ' % field,
                    ' <a href="%s">%s</a> ' % (reverse('table-def', args=[ext_table]), field)
                )
        return definition


class TableListView(TableMixin, TemplateView):
    template_name = "dbdocs/table_list.html"

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        db_tables = [
            DBView(name=model._meta.db_table)
            for model in self.main_model_whitelist
        ]
        lt_tables = [
            DBView(name=model._meta.db_table)
            for model in self.lookup_model_whitelist
        ]
        context.update({
            'main_tables': sorted(set(db_tables)),
            'lookup_tables': sorted(set(lt_tables)),
        })
        return context


class TableDefinitionView(TableMixin, TemplateView):
    template_name = "dbdocs/view_def.html"

    def get(self, request, *args, **kwargs):
        try:
            oid = int(self.kwargs['oid_or_name'])
            self.table = DBView(oid=oid)
        except ValueError:
            self.table = DBView(name=self.kwargs['oid_or_name'])
        if self.table.name is None:
            raise Http404
        # If this table is allowed to display its values, redirect to the view
        # showing tables values.
        lookup_model = next(
            (mod for mod in self.lookup_model_whitelist if mod._meta.db_table == self.table.name),
            None
        )
        if lookup_model:
            return HttpResponseRedirect(reverse('data-model', args=[lookup_model._meta.model_name]))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        with connection.cursor() as cursor:
            context.update({
                'obj_type': "Table",
                'name': self.table.name,
                'comment': self.table.comment,
                'definition': self.decorate_tabledef(self.table),
            })
        return context


class FunctionDefinitionView(TemplateView):
    template_name = "dbdocs/view_def.html"

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT pg_get_functiondef(oid) FROM pg_proc WHERE proname = %s",
                [self.kwargs['func_name']]
            )
            try:
                func_def = cursor.fetchone()[0]
            except TypeError:
                raise Http404
            cursor.execute(
                "SELECT obj_description(%s, 'pg_proc')",
                [func_oid(self.kwargs['func_name'])]
            )
            comment = cursor.fetchone()[0]

        context.update({
            'obj_type': "Function",
            'name': self.kwargs['func_name'],
            'definition': func_def,
            'comment': comment,
        })
        return context


class ModelView(TableMixin, TemplateView):
    """
    View to display the content of a database table/view from a defined model
    """
    template_name = "dbdocs/model_data.html"

    def get_table_content(self, model_name):
        model = apps.get_model(settings.DBDOCS_MAIN_APP_NAME, model_name)
        fields = [
            f for f in model._meta.get_fields()
            if not (f.one_to_many or f.many_to_many) and f.name not in ('id', 'geom')
        ]
        return {
            'field_names': [f.verbose_name for f in fields],
            'data': model.objects.all().values_list(*[f.name for f in fields]),
        }

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        if self.kwargs['model'].lower() not in [
                mod._meta.model_name for mod in self.lookup_model_whitelist]:
            raise Http404
        model = apps.get_model(settings.DBDOCS_MAIN_APP_NAME, self.kwargs['model'])
        fields = [f for f in model._meta.get_fields() if not (f.one_to_many or f.many_to_many) and f.name != 'id']
        table = DBView(name=model._meta.db_table)
        context.update({
            'title': "%s (%s '%s')" % (model._meta.verbose_name, _("Table"), table.name),
            'definition': self.decorate_tabledef(table),
            'comment': table.comment,
        })
        context.update(self.get_table_content(self.kwargs['model']))
        return context


def func_oid(func_name):
    with connection.cursor() as cursor:
        cursor.execute("SELECT oid FROM pg_catalog.pg_proc WHERE proname=%s", [func_name]);
        try:
            oid = cursor.fetchone()[0]
        except TypeError:
            oid = None
    return oid
