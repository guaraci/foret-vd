from django.urls import path

from . import views

urlpatterns = [
    path('view_def/', views.ViewList.as_view(), name='view-list'),
    path('view_def/<int:oid>/', views.ViewDefinition.as_view(), name='view-def'),
    path('table_def/', views.TableListView.as_view(), name='table-list'),
    path('table_def/<slug:oid_or_name>/', views.TableDefinitionView.as_view(), name='table-def'),
    path('func_def/<slug:func_name>/', views.FunctionDefinitionView.as_view(), name='func-def'),
    path('data/<slug:model>/', views.ModelView.as_view(), name='data-model'),
]
