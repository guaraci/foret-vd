CREATE OR REPLACE VIEW public.base_vocabulary_values AS
 SELECT row_number() OVER () AS id,
    plot_obs.id AS plot_obs_id,
    protocol.vocab_posd6_id AS vocab_id,
    plot_obs.reserved3 AS value
   FROM plot_obs
     LEFT JOIN protocol ON plot_obs.protocol_id = protocol.id
  WHERE protocol.vocab_posd6_id IS NOT NULL
UNION
 SELECT row_number() OVER () AS id,
    plot_obs.id AS plot_obs_id,
    protocol.vocab_posd7_id AS vocab_id,
    plot_obs.reserved4 AS value
   FROM plot_obs
     LEFT JOIN protocol ON plot_obs.protocol_id = protocol.id
  WHERE protocol.vocab_posd7_id IS NOT NULL
UNION
 SELECT row_number() OVER () AS id,
    plot_obs.id AS plot_obs_id,
    protocol.vocab_posp8_id AS vocab_id,
    plot_obs.reserved1 AS value
   FROM plot_obs
     LEFT JOIN protocol ON plot_obs.protocol_id = protocol.id
  WHERE protocol.vocab_posp8_id IS NOT NULL
UNION
 SELECT row_number() OVER () AS id,
    plot_obs.id AS plot_obs_id,
    protocol.vocab_posp9_id AS vocab_id,
    plot_obs.reserved2 AS value
   FROM plot_obs
     LEFT JOIN protocol ON plot_obs.protocol_id = protocol.id
  WHERE protocol.vocab_posp9_id IS NOT NULL;
