CREATE OR REPLACE VIEW public.base_gibi_analysis AS
  SELECT plot_obs.id, year, owner_id, gibi_epicea * pobs_weight.surface_weighting AS gibi_epicea_ha,
       gibi_epicea_f * gibi_epicea * pobs_weight.surface_weighting / 100 AS gibi_epicea_f_ha,
       gibi_epicea_a * gibi_epicea * pobs_weight.surface_weighting / 100 AS gibi_epicea_a_ha,
       gibi_conifer * pobs_weight.surface_weighting AS gibi_conifer_ha,
       gibi_conifer_f * gibi_conifer * pobs_weight.surface_weighting / 100 AS gibi_conifer_f_ha,
       gibi_conifer_a * gibi_conifer * pobs_weight.surface_weighting / 100 AS gibi_conifer_a_ha,
       (gibi_epicea + gibi_conifer)::int * pobs_weight.surface_weighting AS gibi_resineux_ha,
       (gibi_epicea_f * gibi_epicea * pobs_weight.surface_weighting / 100) + (gibi_conifer_f * gibi_conifer * pobs_weight.surface_weighting / 100) AS gibi_resineux_f_ha,
       (gibi_epicea_a * gibi_epicea * pobs_weight.surface_weighting / 100) + (gibi_conifer_a * gibi_conifer * pobs_weight.surface_weighting / 100) AS gibi_resineux_a_ha,
       gibi_leaved * pobs_weight.surface_weighting AS gibi_leaved_ha,
       gibi_leaved_f * gibi_leaved * pobs_weight.surface_weighting / 100 AS gibi_leaved_f_ha,
       gibi_leaved_a * gibi_leaved * pobs_weight.surface_weighting / 100 AS gibi_leaved_a_ha,
       (gibi_epicea + gibi_conifer + gibi_leaved)::int * pobs_weight.surface_weighting AS gibi_total_ha,
       (gibi_epicea_f * gibi_epicea * pobs_weight.surface_weighting / 100) + (gibi_conifer_f * gibi_conifer * pobs_weight.surface_weighting / 100) + (gibi_leaved_f * gibi_leaved * pobs_weight.surface_weighting / 100) AS gibi_total_f_ha,
       (gibi_epicea_a * gibi_epicea * pobs_weight.surface_weighting / 100) + (gibi_conifer_a * gibi_conifer * pobs_weight.surface_weighting / 100) + (gibi_leaved_a * gibi_leaved * pobs_weight.surface_weighting / 100) AS gibi_total_a_ha
    FROM plot_obs
    LEFT JOIN (
        SELECT plot_obs.id,
               density.surface/ksp_vd_surface_placette_exacte(4::smallint, plot_obs.slope) AS surface_weighting,
               protocol.year
        FROM plot_obs
        LEFT JOIN density on plot_obs.density_id=density.id
        LEFT JOIN protocol on protocol.id=plot_obs.protocol_id
    ) pobs_weight ON plot_obs.id=pobs_weight.id;
COMMENT ON VIEW base_gibi_analysis IS 'Vue de base pour le calcul du gibier et le nombre de jeunes plants forestiers (épicéa, autres résineux, feuillus). Les surfaces des placettes sont corrigées avec l’inclinaison de la pente (plot_obs.slope).';
