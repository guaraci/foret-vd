CREATE OR REPLACE VIEW base_surf_vol_par_plotobs_diametre_9 AS 
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot.municipality_id,
    plot_obs.interv_int_id,
    plot_obs.main_nature_id,
    plot_obs.owner_id,
    plot_obs.protocol_id,
    COALESCE(subq."nombre de tiges", 0)::int AS "nombre de tiges",
    density.surface::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) AS "RPSTZ",
    10000::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) * COALESCE(subq."nombre de tiges", 0)::double precision AS "nombre de tiges/hectare",
    10000::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) * COALESCE(subq."volume tarif vaudois", 0)::double precision AS "volume (m3) tarif vaudois/hectare",
    COALESCE(subq."surface terriere", 0) / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) AS "surface terriere (m2)/hectare",
    COALESCE(subq."diametre moyen",0.0) AS "diametre moyen",
    ksp_vd_tarif_vaudois_fonction(COALESCE(subq."diametre moyen", 0.0)) AS "volume de l'arbre moyen",
    ksp_vd_surface_terriere(COALESCE(subq."diametre moyen", 0.0)) AS "surface terriere de l'arbre moyen",
    density.surface / 10000.0 AS surface_forestiere_ha
   FROM ( SELECT tree.obs_id,
            count(*) AS "nombre de tiges",
            sum(ksp_vd_tarif_vaudois_fonction(tree.diameter::double precision)) AS "volume tarif vaudois",
            sum(ksp_vd_surface_terriere(tree.diameter::double precision)) AS "surface terriere",
            sqrt(avg(tree.diameter)*avg(tree.diameter)+var_pop(tree.diameter)) AS "diametre moyen"
           FROM tree
          WHERE tree.diameter > 9
          GROUP BY tree.obs_id) subq
     FULL JOIN plot_obs ON subq.obs_id = plot_obs.id
     LEFT JOIN plot ON plot_obs.plot_id = plot.id
     LEFT JOIN density ON plot_obs.density_id = density.id;
