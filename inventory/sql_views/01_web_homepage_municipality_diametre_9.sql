CREATE OR REPLACE VIEW public.web_homepage_municipality_diametre_9 AS 
 SELECT row_number() OVER () AS id,
    base_surf_vol_par_plotobs_diametre_9.protocol_id,
    base_surf_vol_par_plotobs_diametre_9.municipality_id,
    count(*) AS "nombre de placettes",
    avg(base_surf_vol_par_plotobs_diametre_9."nombre de tiges/hectare") AS "nombre de tiges (> 9cm)/hectare",
    avg(base_surf_vol_par_plotobs_diametre_9."volume (m3) tarif vaudois/hectare") AS "volume (m3) tarif vaudois/hectare",
    avg(base_surf_vol_par_plotobs_diametre_9."surface terriere (m2)/hectare") AS "surface terriere (m2)/hectare",
    avg(base_surf_vol_par_plotobs_diametre_9."diametre moyen") AS "diametre moyen (cm2)",
    avg(base_surf_vol_par_plotobs_diametre_9."volume de l'arbre moyen") AS "volume de l'arbre moyen (m3)",
    avg(base_surf_vol_par_plotobs_diametre_9."surface terriere de l'arbre moyen") AS "surface terriere de l'arbre moyen (cm2)",
    avg(base_surf_vol_par_plotobs_diametre_9."RPSTZ") * count(*)::double precision AS "surface forestiere theorique"
   FROM base_surf_vol_par_plotobs_diametre_9
  GROUP BY base_surf_vol_par_plotobs_diametre_9.municipality_id, base_surf_vol_par_plotobs_diametre_9.protocol_id;
