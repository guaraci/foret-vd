CREATE OR REPLACE VIEW public.base_plotobs_calc
AS
 SELECT po.id,
    po.owner_id,
    po.surf_placette,
    prot.year,
    density.surface,
    density.surface::double precision / po.surf_placette AS rpstz,
    COALESCE(po_calc_10.nb_tiges_10, 0) AS nb_tiges_10,
    COALESCE(po_calc_16.nb_tiges_16, 0) AS nb_tiges_16,
    COALESCE(po_calc_10.volume_tarif_vaudois_10, 0) AS volume_tarif_vaudois_10,
    COALESCE(po_calc_16.volume_tarif_vaudois_16, 0) AS volume_tarif_vaudois_16,
    po_calc_10.diametre_moyen_10,
    po_calc_16.diametre_moyen_16,
    COALESCE(po_calc_10.surf_terr_10, 0) AS surf_terr_10,
    COALESCE(po_calc_16.surf_terr_16, 0) AS surf_terr_16,
    COALESCE(ksp_vd_ddom(po_calc_10.obs_id), 1) AS ddom,
    LEAST(6, COALESCE(ksp_vd_ddom(po_calc_10.obs_id), 1) / 10 + 1) AS ddom_class,
    10000::double precision / po.surf_placette * COALESCE(po_calc_10.surf_terr_10, 0::double precision) AS g10,
    10000::double precision / po.surf_placette * COALESCE(po_calc_16.surf_terr_16, 0::double precision) AS g16,
    ksp_vd_surface_terriere(COALESCE(po_calc_10.diametre_moyen_10, 0.0)::double precision) AS surf_terr_moyen_10,
    ksp_vd_surface_terriere(COALESCE(po_calc_16.diametre_moyen_16, 0.0)::double precision) AS surf_terr_moyen_16,
    po_calc_10.diametre_moyen_10::double precision * po.surf_placette AS diametre_moyen_pond_10,
    po_calc_16.diametre_moyen_16::double precision * po.surf_placette AS diametre_moyen_pond_16
   FROM ( SELECT tree.obs_id,
            count(tree.id) as nb_tiges_10,
            sqrt(avg(tree.diameter) * avg(tree.diameter) + var_pop(tree.diameter)) AS diametre_moyen_10,
            sum(ksp_vd_surface_terriere(tree.diameter)) AS surf_terr_10,
            sum(ksp_vd_tarif_vaudois_tableau(tree.diameter)) AS volume_tarif_vaudois_10
           FROM tree
             LEFT JOIN tree_spec ON tree_spec.id = tree.spec_id
          WHERE tree.diameter > 9
          GROUP BY tree.obs_id) po_calc_10
     LEFT JOIN ( SELECT tree2.obs_id,
            count(tree2.id) as nb_tiges_16,
            sqrt(avg(tree2.diameter) * avg(tree2.diameter) + var_pop(tree2.diameter)) AS diametre_moyen_16,
            sum(ksp_vd_surface_terriere(tree2.diameter)) AS surf_terr_16,
            sum(ksp_vd_tarif_vaudois_tableau(tree2.diameter)) AS volume_tarif_vaudois_16
           FROM tree tree2
             LEFT JOIN tree_spec ON tree_spec.id = tree2.spec_id
          WHERE tree2.diameter > 16
          GROUP BY tree2.obs_id) po_calc_16 ON po_calc_16.obs_id = po_calc_10.obs_id
     FULL JOIN ( SELECT plot_obs.id,
            plot_obs.owner_id,
            plot_obs.protocol_id,
            plot_obs.density_id,
            ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) AS surf_placette
           FROM plot_obs) po ON po_calc_10.obs_id = po.id
     LEFT JOIN protocol prot ON prot.id = po.protocol_id
     LEFT JOIN density ON po.density_id = density.id;
