from decimal import Decimal

from django.test import TestCase
from django.urls import reverse

from inventory.models import Intervention, Owner, PlotObs, PlotObsCalc, Vocabulary
from municipality.models import Municipality
from .utils import BaseDataMixin, MockFile


class InventoryTests(BaseDataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        test_f = MockFile(
            'VALL1 I08\r\n'
            'A23607470 B42771070 C152811 D160A2 0100103100  N378225\r\n'
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n'
            ' *57/011. *67/015. *67/022.\r\n'
            'A23507460 B42771072 C152311 D000A4 0110000209  N378226\r\n'
            ' *47/011. *47/013. *47/016. *47/022. *E7/054.')
        num, imp_data = cls.import_data(test_f, 2005)
        assert num == 2

    def test_index_views(self):
        response = self.client.get(reverse('home'))
        self.assertRedirects(response, reverse('login') + '?next=/')
        # Test index views: by owner and by municipality
        self.client.login(username='user', password='password')
        response = self.client.get(reverse('home'))
        self.assertContains(response, '<th>Propriétaire')
        response = self.client.get(reverse('home') + '?by=municipality')
        self.assertContains(response, '<th>Commune')

    def test_plot_obs_precalc(self):
        obs = PlotObs.objects.get(uid=378225)
        obs_calc = PlotObsCalc.objects.get(plot_obs__uid=378225)
        self.assertEqual(obs_calc.ddom, 22)
        self.assertEqual(obs_calc.ddom_class, 3)

    def test_data_nbtiges(self):
        self.client.login(username='user', password='password')
        base_url = reverse('data_grid') + '?bio=Nbtiges9&own=%s&no_aggr=none' % Owner.objects.get(num=277).pk
        response = self.client.get(base_url)
        self.assertEqual(len(response.context['query']), 2)
        # Default aggregation (year)
        base_url = reverse('data_grid') + '?bio=Nbtiges9&own=%s' % Owner.objects.get(num=277).pk
        response = self.client.get(base_url)
        self.assertEqual(len(response.context['query']), 1)
        # Include aggregation
        response = self.client.get(base_url + '&aggr=protocol__year&aggr=interv_int')
        self.assertEqual(len(response.context['query']), 2)
        self.assertIn('Soins culturaux', response.context['query'][0])

    def test_data_nbtiges_year_from_to(self):
        # Duplicate protocol/plot_obs for other year
        protocole = self.protocol
        obss = list(protocole.plotobs_set.all())
        protocole.pk = None
        protocole.year = 1997
        protocole.save()
        for obs in obss:
            obs.pk = None
            obs.uid += 10000
            obs.save()
        self.client.login(username='user', password='password')
        url = reverse('data_grid') + '?bio=Nbtiges9&own=%s&year_from=2003&year_to=2010' % Owner.objects.get(num=277).pk
        response = self.client.get(url)
        self.assertEqual(len(response.context['query']), 1)

    def test_data_boismort_owner(self):
        self.client.login(username='user', password='password')
        base_url = reverse('data_grid') + '?bio=BoisMort&own=%s' % Owner.objects.get(num=277).pk
        response = self.client.get(base_url)
        self.assertEqual(len(response.context['query']), 2)
        # Include aggregation
        response = self.client.get(base_url + '&aggr=municip-owner&aggr=value')
        self.assertEqual(
            response.context['field_names'],
            ['*Propriétaire', '*Année', '*Valeur', 'Valeur', 'Nombre de placettes']
        )
        self.assertQuerysetEqual(
            response.context['query'],
            [('277', 2005, '0', 1, 1), ('277', 2005, '2', 1, 1)], lambda r:tuple(r)
        )

    def test_data_boismort_municip(self):
        self.client.login(username='user', password='password')
        base_url = reverse('data_grid') + '?bio=BoisMort&mun=%s' % Municipality.objects.get(name="Vallorbe").pk
        response = self.client.get(base_url)
        self.assertEqual(len(response.context['query']), 2)
        # Include aggregation
        response = self.client.get(base_url + '&aggr=municip-owner&aggr=value')
        self.assertEqual(
            response.context['field_names'],
            ['*Commune', '*Année', '*Valeur', 'Valeur', 'Nombre de placettes']
        )
        self.assertQuerysetEqual(
            response.context['query'],
            [('Vallorbe', 2005, '0', 1, 1), ('Vallorbe', 2005, '2', 1, 1)], lambda r:tuple(r)
        )

    def test_new_protocol(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('protocole_addpassed'))
        self.assertContains(response, '<label for="id_name">Nom :</label>')
        # TODO: post

    def test_protocol_form(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('protocole_edit', args=[self.protocol.pk]), data={
            'year': '2008', 'inv_date': "Printemps 2005",
            'owners': self.protocol.owners.values_list('pk', flat=True),
            'interv_0': str(self.protocol.interv_0_id),
            'interv_1': str(self.protocol.interv_1_id),
            'interv_2': str(self.protocol.interv_2_id),
            'interv_3': str(Intervention.objects.create(name='Soins divers', code='3').pk),
            'vocab_posp8': str(Vocabulary.objects.get(name='Écorçage du cerf').pk),
            'spec_posE': str(self.protocol.spec_posE_id),
            'default_density': str(self.protocol.default_density_id),
        })
        self.assertRedirects(response, reverse('protocoles'))
        self.protocol.refresh_from_db()
        self.assertEqual(self.protocol.vocab_posp8.name, 'Écorçage du cerf')
        self.assertEqual(self.protocol.interv_3.name, 'Soins divers')

    def test_display_value(self):
        from ..templatetags.display import display_value
        self.assertEqual(display_value(1445.76), '1445,8')
        self.assertEqual(display_value(Decimal(1445.76)), '1445,8')

    def test_as_fbase(self):
        obs = PlotObs.objects.get(uid=378225)
        self.assertEqual(
            obs.as_fbase(),
            '    8U    42771070  23607470  010 2    1 100   5 28  336  11 1 6 23 3            '
            '100103100   378225 67 20   2467 21   2767 25   4347 15   1357 21   2757 11    867 15   1367 22   31'
        )
