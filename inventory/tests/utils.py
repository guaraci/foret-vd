from imports.models import FileImport
from imports.imports import Importer, ImportingError
from inventory.models import (
    Density, Intervention, Owner, OwnerType, PlotObs, Protocol, TreeSpecies,
    User, Vocabulary,
)


class MockFile(object):
    _committed = True
    def __init__(self, content):
        self.name = 'mocked.cor'
        self.content = bytes(content, 'utf-8')
        self.generator = iter(self)

    def __iter__(self):
        for line in self.content.split(b'\r\n'):
            yield line + b'\r\n'

    def seek(self, num):
        self.generator = iter(self)

    def readline(self):
        return next(self.generator)


class BaseDataMixin:
    fixtures = [
        'ownertype.json', 'density.json', 'nature.json', 'treespecies.json',
        'vocabularies.json', 'vallorbe.json'
    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.protocol = Protocol.objects.create(
            year=2005, inv_date="Printemps 2005",
            interv_0=Intervention.objects.create(name='Néant', code='0'),
            interv_1=Intervention.objects.create(name='Plantations', code='1'),
            interv_2=Intervention.objects.create(name='Soins culturaux', code='2'),
            vocab_posp8=Vocabulary.objects.get(name='Bois mort'),
            spec_posE=TreeSpecies.objects.create(species='Bouleau'),
            default_density=Density.objects.get(code=5),
        )
        cls.protocol.owners.set([
            Owner.objects.create(
                typ=OwnerType.objects.get(alpha_code='C'),
                num='277')
        ])
        cls.user = User.objects.create_user(username='user', password='password')

    @classmethod
    def import_data(cls, test_file, year, continue_on_error=False):
        imp_data = FileImport.objects.create(
            ifile=test_file, year=year)
        importer = Importer(imp_data)
        try:
            num = importer.do_import(continue_on_error=continue_on_error)
        except ImportingError:
            num = 0
        return num, imp_data
