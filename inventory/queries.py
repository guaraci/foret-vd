import re
from collections import namedtuple, OrderedDict

from django.contrib.postgres.aggregates import ArrayAgg
from django.db import connection
from django.db.models import F, IntegerField
from django.db.models.aggregates import Avg, Count, Sum, Variance
from django.db.models.functions import Cast, Sqrt
from django.utils.safestring import SafeString

from municipality.models import Arrondissement, Municipality
from .functions import StdErrRel, SurfaceTerriere, TarifVD
from .models import (
    BaseDbViewDiametre16, BaseDbViewDiametre9, BaseGibiAnalysis, Owner, Tree, VocabularyValue
)

Field = namedtuple('Field', ['name', 'accessor', 'fkey_target', 'vname'])

# Fields not accessible on the base view/model itself
ext_fields = {
    'protocol__year': Field(name='protocol__year', accessor='protocol', fkey_target='__year', vname='Année'),
    'plot__center': Field(name='plot__center', accessor='plot', fkey_target='__center', vname='Coordonnées'),
    'plot__municipality': Field(name='plot__municipality', accessor='plot__municipality', fkey_target='__name', vname='Commune'),
    'vocab_value': Field(name='vocab_value', accessor='vocab_value', fkey_target='', vname='Valeur'),
}


class Query:
    # Mapping to get the 'interesting' field on a foreign key join
    fkey_map = {
        'municipality': ('name', 'Commune'),
        'plot': ('id', 'Placette'),
        'center': ('', 'Coordonnées'),
        'owner': ('num', 'Propriétaire'),
        'protocol': ('pfile', 'Protocole'),
        'interv_int': ('name', 'Intervention'),
        'main_nature': ('description', 'Nature principale'),
        'year': ('', 'Année'),
        'group': ('name', 'Nom'),
    }

    def as_dict(self):
        return {key: getattr(self, key, '') for key in [
            'model', 'fields', 'annot_map', 'groupables'
        ]}

    def get_fkey_target(self, fname):
        name = fname.split('__')[-1]
        if name in self.fkey_map and self.fkey_map[name][0]:
            return '__%s' % self.fkey_map[name][0]
        return ''

    def get_fkey_verbose(self, fname):
        return self.fkey_map.get(fname, ('', fname))[1]

    def base_qs(self):
        return self.model.objects.all()

    def build_query(self, perims, aggrs, year_from, year_to, stddev=True):
        context = {'line_cleaner': self.clean_result_line}
        display_field_names = self.fields[:]
        query = self.base_qs()
        year_accessor = 'year' if 'year' in self.fields else 'protocol__year'
        if year_accessor == 'protocol__year' and aggrs and 'year' in aggrs:
            aggrs[aggrs.index('year')] = 'protocol__year'

        # Filter with perimeter selection
        if perims['municips']:
            query = query.filter(**{f'{self.plot_accessor}municipality__pk__in': perims['municips']})
            context['municipalities'] = Municipality.objects.filter(pk__in=perims['municips']).values_list('name', flat=True)
        if perims['owners']:
            query = query.filter(**{f'{self.plot_obs_accessor}owner__pk__in': perims['owners']})
            context['owners'] = Owner.objects.filter(pk__in=perims['owners'])
        if perims['arrs']:
            query = query.filter(**{f'{self.plot_accessor}municipality__arrond__pk__in': perims['arrs']})
            context['arronds'] = Arrondissement.objects.filter(pk__in=perims['arrs'])

        if year_from and year_to:
            query = query.filter(**{f'{self.plot_obs_accessor}{year_accessor}__range': [year_from, year_to]})
            if aggrs is None or year_accessor not in aggrs:
                # If not grouped by date, exclude plotobs whose year is the older and signify context
                plot_field = self.plot_accessor.rstrip('_')
                dup_obs = query.values(plot_field).annotate(
                    num=Count(plot_field),
                    ids=ArrayAgg('plot_obs_id', ordering=(f'-{self.plot_obs_accessor}{year_accessor}'))
                ).filter(num__gt=1)
                results, _ = self._execute_query(dup_obs, [], as_dict=True)
                to_exclude = []
                for line in results:
                    to_exclude.extend(line['ids'][1:])
                if to_exclude:
                    query = query.exclude(**{f'{self.plot_obs_accessor}plot_obs_id__in': to_exclude})
                    context['excluded_count'] = len(to_exclude)

        if aggrs and 'municip-owner' in aggrs:
            replace_by = 'plot__municipality' if perims['municips'] else 'owner'
            idx = aggrs.index('municip-owner')
            aggrs.remove('municip-owner')
            aggrs.insert(idx, f'{self.plot_obs_accessor}{replace_by}')
            for fname in display_field_names:
                if fname.endswith('municipality'):
                    idx = display_field_names.index(fname)
                    display_field_names.remove(fname)
                    display_field_names.insert(idx, f'{self.plot_obs_accessor}{replace_by}')
                    break

        # Choose fields to display
        fields = OrderedDict()
        for fname in display_field_names + (aggrs or []):
            if '__' in fname:
                parts = fname.rsplit('__', 1)
                fields[fname] = Field(name=fname, accessor=parts[0],
                                      fkey_target='__%s%s' % (parts[1], self.get_fkey_target(fname)),
                                      vname=self.get_fkey_verbose(parts[1]))
            else:
                f = self.model._meta.get_field(fname)
                fields[fname] = Field(f.name, f.name, self.get_fkey_target(f.name), f.verbose_name)

        if aggrs is None:
            #order_by = self.model_data['order_by']
            field_list = [f.accessor + f.fkey_target for f in fields.values()]
            context['query'] = query.values_list(*field_list) #.order_by(order_by)
            # Extract verbose names
            context['field_names'] = self.format_field_names([f.vname for f in fields.values()], has_aggrs=False)
        elif not aggrs and year_from and year_to:
            # No chosen groupment -> force aggregate
            # TODO: find a way to get the executed query
            annot_list, field_names = self.build_annotations(fields, stddev=stddev)
            context['query'] = [query.aggregate(*annot_list).values()]
            context['field_names'] = self.format_field_names(field_names)
        else:
            if year_accessor not in aggrs and not (year_from and year_to):
                # Always add year, as grouping and mixing years makes no sense
                if 'value' in aggrs:
                    aggrs.insert(aggrs.index('value'), year_accessor)
                else:
                    aggrs.append(year_accessor)
            if self.model == VocabularyValue and 'value' not in aggrs:
                # For the vocabulary queries, always group by 'value' if there is some grouping
                aggrs.append('value')
            field_names = []
            aggr_crits = []
            for fname in aggrs:
                if fname == year_accessor:
                    aggr_crits.append(f'{self.plot_obs_accessor}{year_accessor}')
                    field_names.append('*Année')
                    continue
                if fname not in fields:
                    for f in fields:
                        if f.endswith(fname):
                            fname = fields[f].name
                aggr_crits.append('%s%s' % (fname, self.get_fkey_target(fname)))
                field_names.append('*%s' % fields[fname].vname)

            annot_list, _field_names = self.build_annotations(fields, stddev=stddev)
            field_names.extend(_field_names)

            query = query.values_list(*aggr_crits).annotate(*annot_list).order_by(*aggr_crits)
            query, sql = self._execute_query(query, aggr_crits)
            context.update({
                'query': query,
                'query_sql': sql,
                'field_names': self.format_field_names(field_names),
            })
        return context

    def format_field_names(self, field_names, has_aggrs=True):
        names = []
        for fname in field_names:
            if fname == 'Observation de placette':
                if has_aggrs:
                    names.append('Nombre de placettes')
                else:
                    names.append(fname)
            else:
                names.append(
                    SafeString(fname.replace(' [', '<br>[').replace('m3', 'm<sup>3</sup>').replace('m2', 'm<sup>2</sup>'))
                )
        return names

    def clean_result_line(self, line):
        return line  # Noop

    def _execute_query(self, query, aggr_crits, as_dict=False):
        """Return (query_or_results, query sql)"""
        if hasattr(self, 'inject_subquery'):
            # Produce raw SQL, so as subqueries can be dynamically injected into query.
            sql, params = query.query.sql_with_params()
            sql = self.inject_subquery(sql, aggr_crits)
            with connection.cursor() as cursor:
                cursor.execute(sql, params)
                if as_dict:
                    columns = [col[0] for col in cursor.description]
                    rows = [
                        dict(zip(columns, row))
                        for row in cursor.fetchall()
                    ]
                else:
                    rows = cursor.fetchall()
            return rows, sql % params
        else:
            return query, str(query.query)


    def build_annotations(self, fields, stddev=False):
        annot_list = []
        field_names = []
        all_aggr_fields = self.annot_map
        for fname, ftuple in fields.items():
            if fname in all_aggr_fields: # and fname not in aggrs:
                target = fields[fname].accessor
                annot_alias = None
                annot_list.append(all_aggr_fields[fname](target))
                if annot_alias:
                    annot_list[-1].source_expressions[0].name = annot_alias
                vname = fields[fname].vname
                #if all_aggr_fields[fname].name.lower() == 'count':
                #    vname += " (compte)"
                #elif all_aggr_fields[fname].name.lower() == 'avg':
                #    vname += " (moyenne)"
                field_names.append(vname)
                if stddev and getattr(all_aggr_fields[ftuple.name], 'name', '') in {'Avg', 'Sum'} and ftuple.name != 'surface_f':
                    annot_list.append(StdErrRel(ftuple.accessor))
                    field_names.append("Err.type [%]")

        # Ajout nombres de placettes et de tiges
        if 'plot_obs' in fields:
            annot_list.append(Count(fields['plot_obs'].name, distinct=True))
            field_names.append(fields['plot_obs'].vname)
        if 'nb_tree' in fields:
            annot_list.append(Sum(fields['nb_tree'].name))
            field_names.append("Nombre de tiges mesurées")
        return annot_list, field_names


class VocabQuery(Query):
    model = VocabularyValue
    plot_obs_accessor = 'plot_obs__'
    plot_accessor = 'plot_obs__plot__'
    fields = ['plot_obs', 'plot_obs__plot__municipality', 'plot_obs__plot', 'plot_obs__protocol__year', 'value']
    annot_map = {'value': Count}
    groupables = ['value']

    def __init__(self, vocab):
        self.vocab = vocab

    def base_qs(self):
        return VocabularyValue.objects.filter(vocab__name=self.vocab)


class NbtigesBaseQuery(Query):
    plot_obs_accessor = ''
    plot_accessor = 'plot__'
    fields = ['plot_obs', 'plot_obs__uid', 'municipality', 'plot', 'plot__center', 'protocol__year',
              'nb_tree', 'nb_tree_ha', 'volume_ha', 'surface_ha', 'surface_f']
    annot_map = {'nb_tree_ha': Avg, 'volume_ha': Avg, 'surface_ha': Avg, 'surface_f': Sum}
    groupables = ['interv_int', 'main_nature', 'group']

    raw_query = """
        SELECT plot_obs.id,
        plot_obs.plot_id,
        plot.municipality_id,
        plot_obs.interv_int_id,
        plot_obs.main_nature_id,
        plot_obs.owner_id,
        plot_obs.protocol_id,
        COALESCE(subq.tiges, 0)::int AS "nombre de tiges",
        density.surface::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) AS "RPSTZ",
        10000::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) * COALESCE(subq.tiges, 0)::double precision AS "nombre de tiges/hectare",
        10000::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) * COALESCE(subq.volume, 0::double precision) AS "volume (m3) tarif vaudois/hectare",
        COALESCE(subq.surface_terriere, 0) / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) AS "surface terriere (m2)/hectare",
        COALESCE(subq.diametre_moyen, 0.0) AS "diametre moyen",
        ksp_vd_tarif_vaudois_fonction(COALESCE(subq.diametre_moyen, 0.0)::double precision) AS "volume de l'arbre moyen",
        ksp_vd_surface_terriere(COALESCE(subq.diametre_moyen, 0.0)::double precision) AS "surface terriere de l'arbre moyen",
        density.surface / 10000.0 AS surface_forestiere_ha
       FROM ($$subquery$$) subq
         FULL JOIN plot_obs ON subq.obs_id = plot_obs.id
         LEFT JOIN plot ON plot_obs.plot_id = plot.id
         LEFT JOIN density ON plot_obs.density_id = density.id
    """

    raw_query_species_group = """
    SELECT (subq2.plot_obs_id || '-'::text) || subq2.species_group_id AS id,
        subq2.plot_id,
        plot.municipality_id,
        subq2.interv_int_id,
        subq2.main_nature_id,
        subq2.owner_id,
        subq2.protocol_id,
        subq.group_id,
        COALESCE(subq.tiges, 0)::int AS "nombre de tiges",
        density.surface::double precision / ksp_vd_surface_placette_exacte(subq2.radius, subq2.slope) AS "RPSTZ",
        10000::double precision / ksp_vd_surface_placette_exacte(subq2.radius, subq2.slope) * COALESCE(subq.tiges, 0)::double precision AS "nombre de tiges/hectare",
        10000::double precision / ksp_vd_surface_placette_exacte(subq2.radius, subq2.slope) * COALESCE(subq.volume, 0::double precision) AS "volume (m3) tarif vaudois/hectare",
        COALESCE(subq.surface_terriere, 0) / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) AS "surface terriere (m2)/hectare",
        COALESCE(subq.diametre_moyen, 0.0) AS "diametre moyen",
        ksp_vd_tarif_vaudois_fonction(COALESCE(subq.diametre_moyen, 0.0)::double precision) AS "volume de l'arbre moyen",
        ksp_vd_surface_terriere(COALESCE(subq.diametre_moyen, 0.0)::double precision) AS "surface terriere de l'arbre moyen",
        density.surface / 10000.0 AS surface_forestiere_ha
    FROM ($$subquery$$) subq
     RIGHT JOIN ( SELECT plot_obs.id AS plot_obs_id,
            plot_obs.plot_id,
            plot_obs.interv_int_id,
            plot_obs.main_nature_id,
            plot_obs.owner_id,
            plot_obs.protocol_id,
            plot_obs.density_id,
            plot_obs.slope,
            plot_obs.radius,
            species_group.id AS species_group_id
           FROM plot_obs,
            species_group) subq2 ON subq2.plot_obs_id = subq.obs_id AND subq2.species_group_id = subq.group_id
     LEFT JOIN plot ON subq2.plot_id = plot.id
     LEFT JOIN density ON subq2.density_id = density.id
    """

    def inject_subquery(self, parent_sql, aggrs):
        by_group = 'group__name' in aggrs
        raw_query = self.raw_query_species_group if by_group else self.raw_query
        subquery = raw_query.replace("$$subquery$$", self.inner_subquery(by_group=by_group))
        # replace FROM "view_name" by FROM (subquery SQL) "view_name"
        return re.sub(r"FROM (\"[A-Za-z0-9_]*\")", f"FROM ({subquery}) \\1", parent_sql)


class Nbtiges16Query(NbtigesBaseQuery):
    model = BaseDbViewDiametre16

    def inner_subquery(self, by_group=False):
        group_by_flds = ['obs', 'spec__group'] if by_group else ['obs']
        tree_subquery = Tree.objects.filter(diameter__gte=16).values(*group_by_flds).annotate(
            tiges=Count('*'),
            volume=Sum(TarifVD('diameter')),
            surface_terriere=Sum(SurfaceTerriere('diameter')),
            diametre_moyen=Sqrt(Avg('diameter') * Avg('diameter') + Variance('diameter'))
        ).values(*(group_by_flds + ['tiges', 'volume', 'surface_terriere', 'diametre_moyen']))
        return str(tree_subquery.query)


class Nbtiges9Query(NbtigesBaseQuery):
    model = BaseDbViewDiametre9

    def inner_subquery(self, by_group=False):
        group_by_flds = ['obs', 'spec__group'] if by_group else ['obs']
        tree_subquery = Tree.objects.filter(diameter__gt=9).values('obs').annotate(
            tiges=Count('*'),
            volume=Sum(TarifVD('diameter')),
            surface_terriere=Sum(SurfaceTerriere('diameter')),
            diametre_moyen=Sqrt(Avg('diameter') * Avg('diameter') + Variance('diameter'))
        ).values(*(group_by_flds + ['tiges', 'volume', 'surface_terriere', 'diametre_moyen']))
        return str(tree_subquery.query)


class GibierQuery(Query):
    model = BaseGibiAnalysis
    plot_obs_accessor = ''
    plot_accessor = 'plot_obs__plot__'
    fields = [
        'plot_obs', 'year', 'owner',
        'gibi_epicea_ha', 'gibi_epicea_f_ha', 'gibi_epicea_a_ha',
        'gibi_conifer_ha', 'gibi_conifer_f_ha', 'gibi_conifer_a_ha',
        'gibi_resineux_ha', 'gibi_resineux_f_ha', 'gibi_resineux_a_ha',
        'gibi_leaved_ha', 'gibi_leaved_f_ha', 'gibi_leaved_a_ha',
        'gibi_total_ha', 'gibi_total_f_ha', 'gibi_total_a_ha',
    ]
    annot_map = {
        'gibi_epicea_ha': Avg, 'gibi_epicea_f_ha': Avg, 'gibi_epicea_a_ha': Avg,
        'gibi_conifer_ha': Avg, 'gibi_conifer_f_ha': Avg, 'gibi_conifer_a_ha': Avg,
        'gibi_resineux_ha': Avg, 'gibi_resineux_f_ha': Avg, 'gibi_resineux_a_ha': Avg,
        'gibi_leaved_ha': Avg, 'gibi_leaved_f_ha': Avg, 'gibi_leaved_a_ha': Avg,
        'gibi_total_ha': Avg, 'gibi_total_f_ha': Avg, 'gibi_total_a_ha': Avg,
    }

    def clean_result_line(self, line):
        # Transform absolute values to % for frottés/abroutis numbers.
        line = list(line)
        for dispos, frottes, abroutis in [
            (1, 3, 5), (7, 9, 11), (13, 15, 17), (19, 21, 23), (25, 27, 29)
        ]:
            line[frottes] = line[frottes] / line[dispos] * 100 if line[dispos] else 0
            line[abroutis] = line[abroutis] / line[dispos] * 100 if line[dispos] else 0
        return line


QUERIES = {
    'Nbtiges16': Nbtiges16Query(),
    'Nbtiges9': Nbtiges9Query(),
    'Gibier': GibierQuery(),
    'BoisMort': VocabQuery('Bois mort'),
    'Myrtilles': VocabQuery('Myrtilles'),
    'Cerf': VocabQuery('Écorcage du cerf'),
}
