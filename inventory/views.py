import csv
import json
from collections import OrderedDict, namedtuple
from datetime import date
from decimal import Decimal
from itertools import chain

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.gis.db.models.aggregates import Collect
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.db.models import Avg, Count, F, IntegerField, Max, Model, Prefetch, Q, Sum
from django.db.models.functions import NullIf, Pi, Round, Sqrt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.generic import (
    CreateView, DeleteView, DetailView, FormView, ListView, TemplateView,
    UpdateView, View
)

from document.models import Document
from municipality.models import Arrondissement, Municipality
from .forms import (
    FBaseForm, FutureProtocolForm, ImportPlotsForm, OwnerAnalysisForm,
    PassedProtocolForm, PlotObsPlannedForm, TopapForm,
)
from .functions import ConvexHull
from .models import (
    BaseGibiAnalysis, BaseSurfTerrSpeciesGT10, BaseTree, BaseVolumeTigesClasseDiam,
    Owner, OwnerType, Plot, PlotObs, PlotObsPlanned, Protocol, Topap, Tree,
    TreeSpecies, User, Vocabulary, WebHomepageMunicipality, WebHomepageOwner,
)
from .queries import QUERIES, Query


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        view_by = self.request.GET.get('by', 'owner')
        if view_by == 'owner':
            base_view = WebHomepageOwner.objects.all(
                ).select_related('owner', 'protocol'
                ).order_by('owner__typ_id', 'owner__num', 'protocol__year')
        else:
            base_view = WebHomepageMunicipality.objects.all(
                ).select_related('municipality', 'protocol'
                ).order_by('municipality__name', 'protocol__year')
        context.update({
            'cur_year': date.today().year,
            'inventaires': Protocol.objects.filter(year=date.today().year),
            'view_by': view_by,
            'data': base_view,
            'metadata': base_view.model._meta,
            'col1_name': "Propriétaire" if view_by == 'owner' else "Commune",
            'col1_descr': "Propriétaire forestier" if view_by == 'owner' else "Commune territoriale",
        })
        if view_by == 'owner':
            context['entities'] = OwnerType.objects.all().order_by('id')
        return context


class HomeEntityData(TemplateView):
    template_name = 'partial/index_table.html'

    def get_context_data(self, **kwargs):
        owner_type = get_object_or_404(OwnerType, pk=self.kwargs['pk'])
        data = WebHomepageOwner.objects.filter(owner__typ=owner_type).all(
                ).select_related('owner', 'protocol'
                ).order_by('owner__num', 'protocol__year')
        return {**super().get_context_data(**kwargs), 'data': data}


class EntityMixin:
    def get_entity(self):
        if self.kwargs['name'] == 'owner':
            return get_object_or_404(Owner, pk=self.kwargs['pk'])
        else:
            return get_object_or_404(Municipality, pk=self.kwargs['pk'])


class EntityView(EntityMixin, TemplateView):
    """Entity is either an owner or a municipality."""
    template_name = 'entity.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        entity = self.get_entity()
        if self.kwargs['name'] == 'owner':
            municipality = None
            if entity.typ.name == 'Commune':
                try:
                    municipality = Municipality.objects.get(lad_nocanc=entity.num)
                except Municipality.DoesNotExist:
                    pass
                except Municipality.MultipleObjectsReturned:
                    municipality = Municipality.objects.get(lad_nocanc=entity.num, fusion_dans__isnull=True)
            plot_obs_base = entity.plotobs_set
            planned_count = entity.plotobsplanned_set.count()
            title = "Propriétaire : %s (%s)" % (entity.name, entity.code)
            entity_geom = entity.plotobs_set.aggregate(geom=ConvexHull(Collect('plot__center')))['geom']
            if entity_geom:
                center = entity_geom.centroid
            else:
                center = Municipality.objects.get(name='Assens').geom.centroid
        else:
            municipality = entity
            plot_obs_base = PlotObs.objects.filter(plot__municipality=municipality)
            planned_count = PlotObsPlanned.objects.filter(plot__municipality=municipality).count()
            title = "Commune : %s" % municipality.name
            entity_geom = municipality.geom
            center = municipality.geom.centroid

        if entity_geom:
            entity_geom.transform(4326)

        context.update({
            'entity': entity,
            'entity_type': self.kwargs['name'],
            'title': title,
            'mun_geom': entity_geom,
            'center': center,
            'plot_obs_by_year': plot_obs_base.values('protocol__year').annotate(Count('id')
                        ).order_by('protocol__year'),
            'plot_obs_url': reverse('plotobs', args=[self.kwargs['name'], entity.pk]),
            'planned_count': planned_count,
        })
        return context


class EntityFbaseExport(EntityMixin, View):
    def get(self, request, *args, **kwargs):
        entity = self.get_entity()
        if kwargs['name'] == 'owner':
            plotobs = PlotObs.objects.filter(
                protocol__year=kwargs['year'], owner_id=kwargs['pk']
            )
        else:
            plotobs = PlotObs.objects.filter(
                protocol__year=kwargs['year'], plot__municipality__id=kwargs['pk']
            )
        response = HttpResponse(content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename="{}-{}.txt"'.format(
            str(entity), kwargs['year']
        )

        plotobs = plotobs.select_related(
            'plot', 'owner__typ', 'main_nature', 'interv_int', 'protocol',
        ).prefetch_related(
            Prefetch('tree_set', queryset=Tree.objects.select_related('spec'))
        )
        for po in plotobs:
            response.write(po.as_fbase() + "\r\n")
        return response


class OwnerAnalysisView(FormView):
    """View to produce 'Tableaux récap.'."""
    template_name = 'owner_analysis.html'
    form_class = OwnerAnalysisForm
    FEUILLU_DIVERS = 'feuillus divers (tous les feuillus qui ne sont pas spécifiés dans cette liste)'
    RESINEUX_DIVERS = 'résineux divers (if, séquoia, thuya, etc.)'
    species = OrderedDict([
        ('épicéa', 'épicéa'), ('sapin', 'sapin'), ('mélèze', 'mélèze'),
        ('douglas', 'dougla'), ('pins, tous les pins', 'pins'),
        (RESINEUX_DIVERS, 'R-div'),
        ('hêtre', 'hêtre'), ('érable sycomore, plane et obier', 'érable'),
        ('chêne pédonculé et sessile', 'chêne'), ('frêne', 'frêne'),
        ('merisier', 'meris'), ("peuplier, tremble, d'Italie, etc.", 'peupl'),
        (FEUILLU_DIVERS, 'F-div'),
    ])
    diam_classes = [
        'N8-15', 'N16-19', 'N20-23', 'N24-27', 'N28-31', 'N32-35', 'N36-39',
        'N40-43', 'N44-47', 'N48-51', 'N52-55', 'N56-59', 'N60-63', 'N64-67',
        'N68-71', 'N72-75', 'N76-79', 'N80+'
    ]

    def get_analysis_data(self, query_filters):
        db_species = {ts.species: ts for ts in TreeSpecies.objects.all()}

        def size_group(diam_class):
            # Petit bois : < 28 cm
            # Bois moyen : >=28 cm et < 48 cm
            # Gros bois : >=48 cm
            if diam_class == 'N8-15':
                return
            if diam_class.endswith(('19', '23', '27')):
                return 'N-peti'
            elif diam_class.endswith(('31', '35', '39', '43', '47')):
                return 'N-moye'
            return 'N-gros'

        def norm_spec_name(name):
            if name not in self.species.keys():
                return self.RESINEUX_DIVERS if db_species[name].typ == 'r' else self.FEUILLU_DIVERS
            return name

        def is_resineux(name):
            return list(self.species.keys()).index(name) <= 5

        def calc_perc(part, tot):
            if tot == 0:
                return 0
            return round(part / tot * 100)

        def calc_tuple_mean(tuples):
            if tuples:
                return (
                    sum(dm * num for dm, num in tuples) /
                    sum(num for dm, num in tuples)
                )
            else:
                return 0

        zero = 0.0
        empty_line = {
            **{sp: zero for sp in self.species.keys()},
            'R/HA': zero, 'F/HA': zero, 'T/HA': zero,
            'R-ABS': 0, 'F-ABS': 0, 'T-ABS': 0,
        }
        d_classes = {
            d_name: empty_line.copy() for d_name in self.diam_classes + [
                'N-peti', 'N-moye', 'N-gros', 'N-T/HA', 'N-Tabs', 'N-%',
                'V-peti', 'V-moye', 'V-gros', 'V-T/HA', 'V-Tabs', 'V-%',
            ]
        }
        # Build filter based on selection form
        filter_plotobs = {key.replace('plot_obs__', ''): val for key, val in query_filters.items()}
        plot_obs = PlotObs.objects.annotate(year=F('imp_file__year')).filter(**filter_plotobs)
        num_plots = plot_obs.count()
        if not num_plots:
            return
        surface = plot_obs.aggregate(Sum('density__surface'))['density__surface__sum'] // 10000

        tiges_vols = BaseVolumeTigesClasseDiam.objects.filter(**query_filters).values(
            'spec', 'species', 'diam_class'
        ).annotate(
            sum_tiges=Sum('num_tiges_real'),
            sum_tiges_ha=Sum('num_tiges_real') / surface,
            sum_vol=Sum('volume_real'),
            sum_vol_ha=Sum('volume_real') / surface,
        )

        for line in tiges_vols:
            if line['spec'] is None:
                continue
            spec_name = norm_spec_name(line['species'])
            diam_class = line['diam_class']
            for line_key in (diam_class, size_group(diam_class)):
                if line_key is None:
                    continue
                d_classes[line_key][spec_name] += line['sum_tiges_ha']
                d_classes[line_key]['T/HA'] += line['sum_tiges_ha']
                d_classes[line_key]['T-ABS'] += line['sum_tiges']
                if is_resineux(spec_name):
                    d_classes[line_key]['R/HA'] += line['sum_tiges_ha']
                    d_classes[line_key]['R-ABS'] += line['sum_tiges']
                else:
                    d_classes[line_key]['F/HA'] += line['sum_tiges_ha']
                    d_classes[line_key]['F-ABS'] += line['sum_tiges']
            if diam_class != 'N8-15':
                d_classes['N-T/HA'][spec_name] += line['sum_tiges_ha']
                d_classes['N-T/HA']['T/HA'] += line['sum_tiges_ha']
                d_classes['N-Tabs'][spec_name] += line['sum_tiges']
                d_classes['N-Tabs']['T/HA'] += line['sum_tiges']
                d_classes['V-T/HA'][spec_name] += line['sum_vol_ha']
                d_classes['V-T/HA']['T/HA'] += line['sum_vol_ha']
                d_classes['V-Tabs'][spec_name] += line['sum_vol']
                d_classes['V-Tabs']['T/HA'] += line['sum_vol']
                v_size_group = size_group(diam_class).replace('N', 'V')
                d_classes[v_size_group][spec_name] += line['sum_vol_ha']
                d_classes[v_size_group]['T-ABS'] += line['sum_vol']
                d_classes[v_size_group]['T/HA'] += line['sum_vol_ha']
                if is_resineux(spec_name):
                    d_classes[v_size_group]['R/HA'] += line['sum_vol_ha']
                    d_classes[v_size_group]['R-ABS'] += line['sum_vol']
                    d_classes['N-T/HA']['R/HA'] += line['sum_tiges_ha']
                    d_classes['N-Tabs']['R/HA'] += line['sum_tiges']
                    d_classes['V-T/HA']['R/HA'] += line['sum_vol_ha']
                    d_classes['V-Tabs']['R/HA'] += line['sum_vol']
                else:
                    d_classes[v_size_group]['F/HA'] += line['sum_vol_ha']
                    d_classes[v_size_group]['F-ABS'] += line['sum_vol']
                    d_classes['N-T/HA']['F/HA'] += line['sum_tiges_ha']
                    d_classes['N-Tabs']['F/HA'] += line['sum_tiges']
                    d_classes['V-T/HA']['F/HA'] += line['sum_vol_ha']
                    d_classes['V-Tabs']['F/HA'] += line['sum_vol']

        # Calculate N-% and V-% percentages
        for key in d_classes['N-Tabs']:
            if key in ['R-ABS', 'F-ABS', 'T-ABS']:
                d_classes['N-Tabs'][key] = d_classes['N-%'][key] = ''
            else:
                d_classes['N-Tabs'][key] = round(d_classes['N-Tabs'][key])
                d_classes['N-%'][key] = calc_perc(d_classes['N-Tabs'][key], int(d_classes['N-Tabs']['T/HA']))
        for key in d_classes['V-Tabs']:
            if key in ['R-ABS', 'F-ABS', 'T-ABS']:
                d_classes['V-Tabs'][key] = d_classes['V-%'][key] = ''
            else:
                d_classes['V-Tabs'][key] = int(d_classes['V-Tabs'][key])
                d_classes['V-%'][key] = calc_perc(d_classes['V-Tabs'][key], int(d_classes['V-Tabs']['T/HA']))

        # Calculate %R/%F/%T
        for key in d_classes:
            if d_classes[key]['T-ABS']:
                if key == 'N8-15':
                    d_classes[key]['%R'] = calc_perc(d_classes[key]['R-ABS'], d_classes[key]['T-ABS'])
                    d_classes[key]['%F'] = calc_perc(d_classes[key]['F-ABS'], d_classes[key]['T-ABS'])
                    d_classes[key]['%T'] = 100
                else:
                    if key.startswith('N'):
                        d_classes[key]['%R'] = calc_perc(d_classes[key]['R-ABS'], d_classes['N-Tabs']['R/HA'])
                        d_classes[key]['%F'] = calc_perc(d_classes[key]['F-ABS'], d_classes['N-Tabs']['F/HA'])
                        d_classes[key]['%T'] = calc_perc(d_classes[key]['T-ABS'], d_classes['N-Tabs']['T/HA'])
                    else:
                        d_classes[key]['%R'] = calc_perc(d_classes[key]['R-ABS'], d_classes['V-Tabs']['R/HA'])
                        d_classes[key]['%F'] = calc_perc(d_classes[key]['F-ABS'], d_classes['V-Tabs']['F/HA'])
                        d_classes[key]['%T'] = calc_perc(d_classes[key]['T-ABS'], d_classes['V-Tabs']['T/HA'])
                d_classes[key]['R-ABS'] = round(d_classes[key]['R-ABS'])
                d_classes[key]['F-ABS'] = round(d_classes[key]['F-ABS'])
                d_classes[key]['T-ABS'] = round(d_classes[key]['T-ABS'])
            else:
                d_classes[key]['%R'] = d_classes[key]['%F'] = ''

        d_classes.update({
            'G>=10': {
                **empty_line, 'R/HA': 0.0, 'F/HA': 0.0, 'T/HA': 0.0, 'R-ABS': '', 'F-ABS': '', 'T-ABS': '',
            },
            'DGM16': {
                **empty_line, 'R/HA': [], 'F/HA': [], 'T/HA': [], 'R-ABS': '', 'F-ABS': '', 'T-ABS': '',
            },
            'V/N16': {
                **empty_line, 'R/HA': [], 'F/HA': [], 'T/HA': [], 'R-ABS': '', 'F-ABS': '', 'T-ABS': '',
            },
            'N/HA<10': {
                **{sp: '' for sp in self.species.keys()},
                'R/HA': 0, 'F/HA': 0, 'T/HA': 0,
            },
        })
        surf_terr_qs = BaseSurfTerrSpeciesGT10.objects.filter(**query_filters).values(
            'spec', 'species'
        ).annotate(surf_terr=Sum(F('g10') / num_plots / 10000))
        for line in surf_terr_qs:
            spec_name = norm_spec_name(line['species'])
            surf_terr = line['surf_terr']
            d_classes['G>=10'][spec_name] = surf_terr
            if is_resineux(spec_name):
                d_classes['G>=10']['R/HA'] += surf_terr
            else:
                d_classes['G>=10']['F/HA'] += surf_terr
            d_classes['G>=10']['T/HA'] += surf_terr

        dgm16_qs = BaseTree.objects.filter(**{**query_filters, 'diameter__gte': 16}).values(
            'spec', 'species'
        ).annotate(
            diametre_moyen=Sqrt(4 / Pi() * Sum(F('diameter') * F('diameter') * Pi() / 4 * F('repr_fact')) / Sum('repr_fact')),
            num_trees=Count('spec')
        )
        for line in dgm16_qs:
            spec_name = norm_spec_name(line['species'])
            d_classes['DGM16'][spec_name] = line['diametre_moyen']
            if is_resineux(spec_name):
                d_classes['DGM16']['R/HA'].append((line['diametre_moyen'], line['num_trees']))
            else:
                d_classes['DGM16']['F/HA'].append((line['diametre_moyen'], line['num_trees']))
            d_classes['DGM16']['T/HA'].append((line['diametre_moyen'], line['num_trees']))
        d_classes['DGM16']['R/HA'] = calc_tuple_mean(d_classes['DGM16']['R/HA'])
        d_classes['DGM16']['F/HA'] = calc_tuple_mean(d_classes['DGM16']['F/HA'])
        d_classes['DGM16']['T/HA'] = calc_tuple_mean(d_classes['DGM16']['T/HA'])

        vn16_qs = BaseTree.objects.filter(**{**query_filters, 'diameter__gte': 16}).values('species').annotate(
            volume_avg_pond=Sum('volume_pond') / Sum('repr_fact'),
            num_trees=Count('species'),
        )
        for line in vn16_qs:
            spec_name = norm_spec_name(line['species'])
            d_classes['V/N16'][spec_name] = line['volume_avg_pond']
            if is_resineux(spec_name):
                d_classes['V/N16']['R/HA'].append((line['volume_avg_pond'], line['num_trees']))
            else:
                d_classes['V/N16']['F/HA'].append((line['volume_avg_pond'], line['num_trees']))
            d_classes['V/N16']['T/HA'].append((line['volume_avg_pond'], line['num_trees']))
        d_classes['V/N16']['R/HA'] = calc_tuple_mean(d_classes['V/N16']['R/HA'])
        d_classes['V/N16']['F/HA'] = calc_tuple_mean(d_classes['V/N16']['F/HA'])
        d_classes['V/N16']['T/HA'] = calc_tuple_mean(d_classes['V/N16']['T/HA'])

        nha10_qs = BaseTree.objects.filter(**{**query_filters, 'diameter__lte': 10}).values('typ').annotate(
            num_trees_ha=Round(Count('id') / float(num_plots) * 1000.0)
        )
        for line in nha10_qs:
            d_classes['N/HA<10'][f'{line["typ"].upper()}/HA'] = int(line['num_trees_ha'])
            d_classes['N/HA<10']['T/HA'] += int(line['num_trees_ha'])

        gibier = BaseGibiAnalysis.objects.filter(**query_filters).aggregate(
            epicea_avail=Round(Avg('gibi_epicea_ha')),
            epicea_f_perc=Round(Sum('gibi_epicea_f_ha') / NullIf(Sum('gibi_epicea_ha'), 0) * 100),
            epicea_a_perc=Round(Sum('gibi_epicea_a_ha') / NullIf(Sum('gibi_epicea_ha'), 0) * 100),
            conifer_avail=Round(Avg('gibi_conifer_ha')),
            conifer_f_perc=Round(Sum('gibi_conifer_f_ha') / NullIf(Sum('gibi_conifer_ha'), 0) * 100),
            conifer_a_perc=Round(Sum('gibi_conifer_a_ha') / NullIf(Sum('gibi_conifer_ha'), 0) * 100),
            resineux_avail=Round(Avg('gibi_resineux_ha')),
            resineux_f_perc=Round(Sum('gibi_resineux_f_ha') / NullIf(Sum('gibi_resineux_ha'), 0) * 100),
            resineux_a_perc=Round(Sum('gibi_resineux_a_ha') / NullIf(Sum('gibi_resineux_ha'), 0) * 100),
            leaved_avail=Round(Avg('gibi_leaved_ha')),
            leaved_f_perc=Round(Sum('gibi_leaved_f_ha') / NullIf(Sum('gibi_leaved_ha'), 0) * 100),
            leaved_a_perc=Round(Sum('gibi_leaved_a_ha') / NullIf(Sum('gibi_leaved_ha'), 0) * 100),
            total_avail=Round(Avg('gibi_total_ha')),
            total_f_perc=Round(Sum('gibi_total_f_ha') / NullIf(Sum('gibi_total_ha'), 0) * 100),
            total_a_perc=Round(Sum('gibi_total_a_ha') / NullIf(Sum('gibi_total_ha'), 0) * 100),
        )
        return {
            'annee': self.kwargs['year'],
            'surface': surface,
            'placettes': num_plots,
            'lines': d_classes,
            'species': self.species,
            'sep_before': ['N16-19', 'N-peti', 'N-T/HA', 'V-peti', 'V-T/HA', 'G>=10'],
            'gibier': gibier,
        }

    def get_query_filters(self, form_data):
        filter_base = {'year': self.kwargs['year'], 'owner': self.owner}
        if form_data['surface'] == 'boisee':
            filter_base['plot_obs__main_nature__woods'] = True
        if form_data['intervention']:
            filter_base['plot_obs__interv_int'] = form_data['intervention']
        if form_data['classe_ddom']:
            filter_base['plot_obs__plotobscalc__ddom_class'] = form_data['classe_ddom']
        return filter_base

    def form_valid(self, form):
        self.owner = get_object_or_404(Owner, pk=self.kwargs['pk'])
        query_filters = self.get_query_filters(form.cleaned_data)
        analysis_data = self.get_analysis_data(query_filters)
        if not analysis_data:
            return HttpResponse('<p class="errors">Aucune donnée ne correspond à votre sélection.</p>')
        return render(
            self.request, 'owner_analysis_sheet.html', context={**analysis_data, 'params': form.cleaned_data}
        )

    def form_invalid(self, form):
        return HttpResponse(form.errors.as_ul())

    def get_context_data(self, **kwargs):
        self.owner = get_object_or_404(Owner, pk=self.kwargs['pk'])
        return {
            **super().get_context_data(**kwargs),
            'annee': self.kwargs['year'],
            'owner': self.owner,
        }


class PlotObsDataView(View):
    """
    Return a GeoJSON structure containing all PlotObs objects for either an owner
    or a municipality and a year.
    """
    def get(self, request, *args, **kwargs):
        year = request.GET.get('year')
        if year == 'planned':
            plotobs = PlotObsPlanned.objects.select_related('plot'
                ).filter(owner_id=kwargs['pk'])
        elif kwargs['name'] == 'owner':
            plotobs = PlotObs.objects.select_related('plot'
                ).filter(protocol__year=year, owner_id=kwargs['pk'])
        else:
            plotobs = PlotObs.objects.select_related('plot'
                ).filter(protocol__year=year, plot__municipality__id=kwargs['pk'])
        struct = {"type": "FeatureCollection", "features": []}
        for obs in plotobs:
            struct['features'].append({
                "type": "Feature",
                "geometry": json.loads(obs.plot.center.transform(4326, clone=True).json),
                "properties": {
                    "pk": obs.pk,
                    "url": reverse('plotobs_planned_detail' if year == 'planned' else 'plotobs_detail', args=[obs.pk])},
            })
        return JsonResponse(struct)


class PlotObsDetailJSONView(View):
    def get(self, request, *args, **kwargs):
        plotobs = get_object_or_404(PlotObs, pk=kwargs['pk'])
        srid = self.request.GET.get('srid')
        return JsonResponse(plotobs.as_geojson(srid=srid))


class PlotObsDetailView(TemplateView):
    output = 'json'
    model = PlotObs

    def get_template_names(self):
        return 'plotobs_detail_core.html' if 'X-Partial' in self.request.headers else 'plotobs_detail.html'

    def get_context_data(self, **context):
        self.plotobs = get_object_or_404(self.model.objects.select_related('plot'), pk=self.kwargs['pk'])
        srid = self.request.GET.get('srid')
        center_geom = self.plotobs.plot.center.clone()
        if srid is not None:
            center_geom.transform(srid)
        return {
            'geojson': self.plotobs.as_geojson(srid=4326, verbose=True),
            'center': center_geom,
            'plotobs': self.plotobs,
            'hidden_props': ('id', 'type', 'uid'),
        }

    def render_to_response(self, context, **kwargs):
        if self.output == 'json':
            return JsonResponse(context['geojson'])
        else:
            plot = self.plotobs.plot
            props = context['geojson']['features'][0]["properties"].copy()
            props.update({
                'Altitude': plot.sealevel,
            })
            context.update({
                'properties': props,
                'geojson': context['geojson'],
                'gemeinde': self.plotobs.plot.municipality,
                'treeobs': self.plotobs.tree_set.select_related('spec').all(),
                'siblings': self.plotobs.plot.plotobs_set.exclude(pk=self.plotobs.pk),
            })
            return super().render_to_response(context, **kwargs)


class PlotObsPlannedDetailView(DetailView):
    model = PlotObsPlanned
    template_name = 'partial/plotobs_planned.html'

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        if request.session.get('PointModified', False):
            response['HX-Trigger-After-Settle'] = 'PointModified'
            request.session['PointModified'] = False
        return response

    def get_context_data(self, **context):
        return {
            **super().get_context_data(**context),
            'history': self.object.plot.plotobs_set.all().order_by('protocol__year'),
            'topap': Topap.objects.filter(polygon__contains=self.object.plot.center).first(),
        }


class PlotObsPlannedEditView(PermissionRequiredMixin, UpdateView):
    model = PlotObsPlanned
    form_class = PlotObsPlannedForm
    template_name = 'partial/plotobs_planned_form.html'
    permission_required = 'inventory.change_plotobsplanned'

    def get_success_url(self):
        return reverse('plotobs_planned_detail', args=[self.object.pk])

    def form_valid(self, form):
        response = super().form_valid(form)
        self.request.session['PointModified'] = True
        return response


class PlotObsPlannedMultiEditView(PermissionRequiredMixin, View):
    permission_required = 'inventory.change_plotobsplanned'

    def post(self, request, *args, **kwargs):
        protocol = get_object_or_404(Protocol, pk=self.kwargs['prot_pk'])
        subm_form = PlotObsPlannedForm(protocol=protocol, data=request.POST)
        point_ids = request.POST['point_ids'].split(',')
        points = PlotObsPlanned.objects.in_bulk(point_ids).values()
        if subm_form.is_valid():
            for field, value in subm_form.cleaned_data.items():
                if value is not None:
                    for pt in points:
                        setattr(pt, field, value)
                        pt.save()
            messages.success(request, 'Les points ont été modifiés avec succès')
        response = render(request, 'partial/plotobs_planned_form_multi.html', {
            'protocol': protocol, 'empty_form': PlotObsPlannedForm(protocol=protocol),
        })
        response['HX-Trigger-After-Settle'] = 'PointModified'
        return response


class PlotObsPlannedMultiDeleteView(PermissionRequiredMixin, View):
    permission_required = 'inventory.change_plotobsplanned'

    def post(self, request, *args, **kwargs):
        protocol = get_object_or_404(Protocol, pk=self.kwargs['prot_pk'])
        point_ids = request.POST['point_ids'].split(',')
        PlotObsPlanned.objects.filter(pk__in=point_ids).delete()
        response = render(request, 'partial/plotobs_planned_form_multi.html', {
            'protocol': protocol, 'empty_form': PlotObsPlannedForm(protocol=protocol),
        })
        response['HX-Trigger-After-Settle'] = 'PointModified'
        return response


class PlotObsPlannedNewView(PermissionRequiredMixin, View):
    permission_required = 'inventory.add_plotobsplanned'

    def post(self, request, *args, **kwargs):
        protocol = get_object_or_404(Protocol, pk=self.kwargs['prot_pk'])
        center = Point(int(request.POST.get('latitude')), int(request.POST.get('longitude')), srid=2056)
        try:
            plot = Plot.objects.get(center=center)
        except Plot.DoesNotExist:
            plot = Plot.objects.create(
                center=center, municipality=Municipality.get_from_point(center)
            )
        planned = PlotObsPlanned.objects.create(plot=plot, protocol=protocol)
        return JsonResponse({
            'result': 'OK',
            'feature': {
                "type": "Feature",
                "id": planned.pk,
                "properties": {
                    "id": planned.pk,
                    "url": planned.detail_url(),
                    "owner": None,
                    "serie": None,
                    "division": None,
                    "operator": None,
                },
                "geometry": json.loads(planned.plot.center.transform(4326, clone=True).json),
            }
        }, safe=False)


class PlotObsPlannedDeleteView(PermissionRequiredMixin, DeleteView):
    model = PlotObsPlanned
    permission_required = 'inventory.delete_plotobsplanned'

    def delete(self, *args, **kwargs):
        self.get_object().delete()
        return JsonResponse({'result': 'OK'}, safe=False)


class PermissiveEncoder(json.JSONEncoder):
    """
    Custom encoder to simply return 'null' for unserializable objects.
    """
    def default(self, obj):
        if isinstance(obj, type) and issubclass(obj, Model):
            return dict([(f.name, f.verbose_name) for f in obj._meta.fields])
        try:
            return super().default(obj)
        except TypeError:
            return None


class DataPageView(TemplateView):
    template_name = 'data_page.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context.update({
            'municipalities': Municipality.objects.all().order_by('name'),
            'owners': Owner.objects.all().order_by('typ__alpha_code', 'num'),
            'arronds': Arrondissement.objects.all().order_by('no'),
            'views': json.dumps({k: v.as_dict() for k, v in QUERIES.items()}, cls=PermissiveEncoder),
        })
        return context


class DataGridView(TemplateView):
    template_name = 'data_grid.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)

        analyze = self.request.GET.get('bio')
        if not analyze:
            context['error'] = "Erreur: vous devez choisir une donnée d'analyse"
            return context
        elif analyze not in QUERIES:
            context['error'] = "Erreur: l'analyse '%s' n'est pas connue du système" % analyze
            return context
        self.query = QUERIES[analyze]

        # Compute data after request.GET
        if self.request.GET.get('no_aggr') == 'none':
            aggrs = None
        else:
            aggrs = self.request.GET.getlist('yaggr') + self.request.GET.getlist('aggr')
        perims = {
            'municips': [int(v) for v in self.request.GET.getlist('mun')],
            'owners': [int(v) for v in self.request.GET.getlist('own')],
            'arrs': [int(v) for v in self.request.GET.getlist('arr')],
        }
        if not any(perims.values()):
            context['error'] = "Erreur: Vous devez sélectionner au moins une commune, propriétaire ou arrondissement"
            return context
        # To be valid, year_from AND year_to must be both between 1950-2050
        try:
            year_from = int(self.request.GET.get('year_from'))
            year_to = int(self.request.GET.get('year_to'))
            if not (1950 < year_from < 2050 and 1950 < year_to < 2050):
                year_from = year_to = None
        except (TypeError, ValueError):
            year_from = year_to = None
        context.update(self.query.build_query(perims, aggrs, year_from, year_to))
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('format') == 'csv' and 'error' not in context:
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="vd-data-export.csv"'
            writer = csv.writer(response, delimiter=";")
            writer.writerow(context['field_names'])
            for line in context['query']:
                writer.writerow([to_csv(val) for val in context['line_cleaner'](line)])
            return response
        return super().render_to_response(context, **response_kwargs)


class ProtocolesView(ListView):
    model = Protocol
    template_name = 'protocoles.html'

    def get_queryset(self):
        qs = super().get_queryset(
            ).annotate(Count('plotobs')
            ).prefetch_related(Prefetch('owners',
                queryset=Owner.objects.select_related('typ').order_by('typ__alpha_code', 'num'))
            ).order_by('-year', 'pfile')
        if self.request.GET.get('draft'):
            qs = qs.filter(draft=True)
        return qs


class ProtocolesByOwnerView(ListView):
    template_name = 'protocoles_by_owner.html'

    def get_queryset(self):
        prot_qs = Protocol.objects.annotate(Count('plotobs')).order_by('-year')
        if self.request.GET.get('draft'):
            prot_qs = prot_qs.filter(draft=True)
        return Owner.objects.all().prefetch_related(Prefetch('protocol_set', queryset=prot_qs)
            ).order_by('typ', 'num')


class ProtocoleDetailView(DetailView):
    model = Protocol
    template_name = 'protocole.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Count plots by owner
        owners1 = {
            res['owner']: res['id__count']
            for res in self.object.plotobs_set.values('owner').annotate(Count('id'))
        }
        owners2 = {
            res['owner']: res['id__count']
            for res in self.object.plotobsplanned_set.values('owner').annotate(Count('id'))
        }
        context.update({
            'owners': {
                owner: owners1.get(owner.pk, 0) + owners2.get(owner.pk, 0)
                for owner in Owner.objects.filter(
                    pk__in=list(set(owners1.keys()) | set(owners2.keys()))
                ).order_by('typ', 'num')
            },
            'owners_none': self.object.plotobsplanned_set.filter(owner__isnull=True).count(),
            'operators': User.objects.all(),
            'empty_form': PlotObsPlannedForm(protocol=self.object),
        })
        return context


class ProtocoleCreateView(CreateView):
    model = Protocol
    template_name = 'protocole_edit.html'
    typ = None

    @property
    def form_class(self):
        return PassedProtocolForm if self.typ == 'passed' else FutureProtocolForm

    def get_template_names(self):
        return ['protocole_edit.html'] if self.typ == 'passed' else ['protocole_new.html']

    def get_success_url(self):
        return reverse('protocole', args=[self.object.pk])


class ProtocoleUpdateView(UpdateView):
    model = Protocol
    template_name = 'protocole_edit.html'

    @property
    def form_class(self):
        return PassedProtocolForm if self.object.passed else FutureProtocolForm

    def get_success_url(self):
        return reverse('protocoles') if self.object.passed else reverse('protocole', args=[self.object.pk])


class ProtocolePlotsView(View):
    def get_points(self):
        return chain(
            self.inventory.plotobs_set.all().select_related('plot', 'owner', 'owner__typ'),
            self.inventory.plotobsplanned_set.all().select_related('plot', 'owner', 'owner__typ')
        )

    def get(self, *args, **kwargs):
        self.inventory = get_object_or_404(Protocol, pk=kwargs['pk'])
        geojson = {"type": "FeatureCollection", "features": []}
        for plotobs in self.get_points():
            operator = getattr(plotobs, 'operator', None)
            is_planned = plotobs.__class__.__name__ == 'PlotObsPlanned'
            geojson["features"].append({
                "type": "Feature",
                "id": plotobs.pk,
                "properties": {
                    "id": plotobs.pk,
                    "url": plotobs.detail_url(),
                    "owner": plotobs.owner.code if plotobs.owner_id else '',
                    "owner_id": plotobs.owner.pk if plotobs.owner_id else 'none',
                    "serie": plotobs.ser_num,
                    "division": plotobs.division,
                    "owner_top": plotobs.owner_top.code if (is_planned and plotobs.owner_top_id) else 'none',
                    "serie_top": plotobs.ser_num_top if is_planned else '',
                    "division_top": plotobs.division_top if is_planned else '',
                    "operator_id": operator.pk if operator else None,
                    "operator_name": str(operator) if operator else None,
                    "planned": is_planned,
                    "checked": getattr(plotobs, 'checked', False),
                },
                "geometry": json.loads(plotobs.plot.center.transform(4326, clone=True).json),
            })
        return JsonResponse(geojson)


class ProtocoleConflictPlotsView(ProtocolePlotsView):
    """Return only points where own data differ from Topap data."""
    def get_points(self):
        query = self.inventory.plotobsplanned_set.exclude(
            owner_id=F('owner_top_id'), ser_num=F('ser_num_top'), div_num=F('div_num_top')
        )
        return query

    def get_points_diff_with_layer(self):
        '''Old query checking with topap layer. Now unused.'''
        from django.db.models import OuterRef, Subquery
        query = self.inventory.plotobsplanned_set.all().annotate(
            topap_id=Subquery(Topap.objects.filter(polygon__contains=OuterRef('plot__center')).values('pk')[:1])
        ).exclude(topap_id__isnull=True).select_related('plot', 'owner', 'owner__typ')
        topaps = {}

        for pobs in query:
            if not pobs.topap_id in topaps:
                topaps[pobs.topap_id] = Topap.objects.get(pk=pobs.topap_id)
            if (
                topaps[pobs.topap_id].propri != (pobs.owner.code if pobs.owner else '') or
                int_or_none(topaps[pobs.topap_id].serie) != pobs.ser_num or
                int_or_none(topaps[pobs.topap_id].division) != pobs.div_num
            ):
                yield pobs


class TopapGeoJSONView(View):
    def get(self, *args, **kwargs):
        inventory = get_object_or_404(Protocol, pk=kwargs['pk'])
        geojson = {"type": "FeatureCollection", "features": []}
        buf = inventory.envelope().simplify(tolerance=1).buffer(1000)
        for item in Topap.objects.filter(polygon__intersects=buf):
            geojson["features"].append({
                "type": "Feature",
                "id": item.pk,
                "properties": {
                    "id": item.pk,
                    "owner": item.propri,
                    "serie": item.serie,
                    "division": item.division,
                    "triage": item.triage_ge,
                },
                "geometry": json.loads(item.polygon.transform(4326, clone=True).json),
            })
        return JsonResponse(geojson)


class ProtocoleImportPlotsView(FormView):
    template_name = 'protocole_import.html'
    form_class = ImportPlotsForm

    def dispatch(self, request, *args, **kwargs):
        self.protocol = get_object_or_404(Protocol, pk=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **context):
        return {
            **super().get_context_data(**context),
            'protocol': self.protocol,
        }

    def form_valid(self, form):
        try:
            results = form.import_plots(self.protocol)
        except Exception as err:
            if settings.DEBUG:
                raise
            messages.error(self.request, str(err))
        else:
            msg = f"{results['imported']} placettes ont été importées"
            if results['already']:
                msg += f". {results['already']} placettes étaient déjà créées"
            messages.success(self.request, msg)
        return HttpResponseRedirect(reverse('protocole', kwargs=self.kwargs))


class RFInfoView(View):
    """Parse http://www.rfinfo.vd.ch content and return structured JSON."""
    def get(self, request, *args, **kwargs):
        import httpx
        from bs4 import BeautifulSoup
        r = httpx.get('http://www.rfinfo.vd.ch/rfinfo.php?no_commune=%s&no_immeuble=%s' % (
            request.GET.get('no_commune'), request.GET.get('no_immeuble'))
        )
        soup = BeautifulSoup(r.text, features="html.parser")

        is_propri = is_genre = False
        propris = []
        genre = ''
        for tr in soup.find_all('tr'):
            if tr.td and tr.td.text.startswith('Propriétaire(s)'):
                is_propri = True
            elif tr.td and tr.td.text.startswith('Genre(s) de nature et bâtiment(s)'):
                is_genre = True
            elif tr.td and tr.td.text.startswith('Conditions d'):
                is_propri = False
            else:
                if is_propri:
                    propris.append(tr.td.text)
                elif is_genre:
                    genre = tr.td.text
                    is_genre = False

        return JsonResponse({'owners': propris, 'genre': genre})


class VocabView(DetailView):
    model = Vocabulary
    template_name = 'vocabulary_detail.html'


class DocumentationView(TemplateView):
    template_name = "docs.html"

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context['docs'] = Document.objects.all().order_by('-weight')
        return context


class EssencesView(TemplateView):
    template_name = "essences.html"

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context.update({
            'actives': TreeSpecies.objects.filter(active=True).order_by('order'),
            'inactives': TreeSpecies.objects.filter(active=False),
        })
        return context


class EssencesExportView(View):
    def get(self, request, *args, **kwargs):
        content = '\r\n'.join([ts.abbrev for ts in TreeSpecies.objects.filter(active=True).order_by('order')])
        response = HttpResponse(content, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=SPCNAME.TXT'
        return response


class FBaseComparisonView(FormView):
    template_name = 'fbase-comparison.html'
    form_class = FBaseForm

    def form_valid(self, form):
        result = form.check_fbases()
        return self.render_to_response(self.get_context_data(form=form, result=result))


class TopApImportView(FormView):
    template_name = 'import_topap.html'
    form_class = TopapForm

    def form_valid(self, form):
        try:
            form.import_topap()
        except Exception as err:
            messages.error(self.request, "Erreur d'importation du fichier: %s" % err)
        else:
            messages.success(self.request, "Le fichier Topap a été importé avec succès")
        return HttpResponseRedirect(self.request.path)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['last_modif'] = Topap.objects.aggregate(last_mod=Max('date_modif'))['last_mod']
        return context


def to_csv(value):
    if value is None:
        return ''
    return value


def int_or_none(val):
    try:
        return int(val)
    except ValueError:
        return None
