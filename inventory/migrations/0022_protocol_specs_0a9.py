from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0021_protocol_passed'),
    ]

    operations = [
        migrations.AddField(
            model_name='protocol',
            name='spec_pos1',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position 1'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_pos2',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position 2'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_pos3',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position 3'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_pos4',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position 4'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_pos5',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position 5'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_pos6',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position 6'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_pos7',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position 7'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_pos8',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position 8'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_pos9',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position 9'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_posA',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position A'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_posB',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position B'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_posC',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position C'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_posD',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position D'),
        ),
        migrations.AlterField(
            model_name='protocol',
            name='spec_posE',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position E'),
        ),
        migrations.AlterField(
            model_name='protocol',
            name='spec_posF',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.TreeSpecies', verbose_name='Essence en position F'),
        ),
    ]
