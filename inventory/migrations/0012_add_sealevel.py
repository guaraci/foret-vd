from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0011_gibi_fields_nullable'),
    ]

    operations = [
        migrations.AddField(
            model_name='plot',
            name='sealevel',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Altitude'),
        ),
    ]
