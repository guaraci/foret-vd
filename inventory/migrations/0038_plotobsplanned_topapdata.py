from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0037_plotobsplanned_checked_remark'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobsplanned',
            name='div_num_top',
            field=models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='N° division'),
        ),
        migrations.AddField(
            model_name='plotobsplanned',
            name='owner_top',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='inventory.owner', verbose_name='Propriétaire'),
        ),
        migrations.AddField(
            model_name='plotobsplanned',
            name='ser_num_top',
            field=models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='N° série'),
        ),
    ]
