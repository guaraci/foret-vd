from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0010_alter_fields'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plotobs',
            name='gibi_conifer',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Nombre total d’autres résineux'),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='gibi_conifer_a',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Proportion d’autres résineux abroutis'),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='gibi_conifer_f',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Proportion d’autres résineux frotté'),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='gibi_epicea',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Nombre total d’épicéas'),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='gibi_epicea_a',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Proportion d’épicéa abroutis'),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='gibi_epicea_f',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Proportion d’épicéa frotté'),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='gibi_leaved',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Nombre total de feuillus'),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='gibi_leaved_a',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Proportion de feuillus abroutis'),
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='gibi_leaved_f',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Proportion de feuillus frotté'),
        ),
    ]
