from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0008_protocol_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobs',
            name='remarks',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
    ]
