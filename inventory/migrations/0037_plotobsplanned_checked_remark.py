from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0036_plot_obs_calculated'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobsplanned',
            name='checked',
            field=models.BooleanField(default=False, verbose_name='Vérifié'),
        ),
        migrations.AddField(
            model_name='plotobsplanned',
            name='remark',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AlterField(
            model_name='plotobsplanned',
            name='div_num',
            field=models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='N° division'),
        ),
        migrations.AlterField(
            model_name='plotobsplanned',
            name='ser_num',
            field=models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='N° série'),
        ),
    ]
