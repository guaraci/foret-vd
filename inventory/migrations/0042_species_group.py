from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0041_plotobs_gps_point'),
    ]

    operations = [
        migrations.CreateModel(
            name='SpeciesGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25, unique=True, verbose_name="Nom")),
            ],
            options={
                'db_table': 'species_group',
                'verbose_name': 'Groupe d’essences',
                'verbose_name_plural': 'Groupes d’essences',
            },
        ),
        migrations.AddField(
            model_name='treespecies',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='inventory.speciesgroup', verbose_name="Groupement"),
        ),
    ]
