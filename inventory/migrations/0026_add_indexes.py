from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0025_unmanaged_models'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='owner',
            index=models.Index(fields=['num'], name='num_index'),
        ),
        migrations.AddIndex(
            model_name='ownertype',
            index=models.Index(fields=['alpha_code'], name='alpha_code'),
        ),
    ]
