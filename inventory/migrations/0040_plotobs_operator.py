from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0039_view_models'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobs',
            name='operator',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Opérateur'),
        ),
    ]
