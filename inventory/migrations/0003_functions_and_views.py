from pathlib import Path
from django.db import models, migrations

sql_dir = Path(__file__).parent.parent / 'sql_views'


def read_sql(dir_):
    for path in sorted(dir_.glob('*.sql')):
        with path.open('r') as fh:
            yield fh.read()


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0007_density_surface'),
    ]

    operations = [
        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_vd_surface_terriere(diameter double precision)
  RETURNS double precision AS
$BODY$
  SELECT pi() * ($1 * $1) / 4 AS surface_terriere_arbre;
$BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;""", "DROP FUNCTION ksp_vd_surface_terriere(diameter double precision)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_vd_surface_placette_exacte(radius smallint, slope smallint)
  RETURNS double precision AS
$BODY$
  SELECT pi() * $1 * $1 * cos($2 / (pi() * 180)) AS "surface placette exacte_sans corr_0.5";
$BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;""", "DROP FUNCTION ksp_vd_surface_placette_exacte(radius smallint, slope smallint)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_vd_tarif_vaudois_fonction(diameter double precision)
  RETURNS double precision AS
$BODY$
  SELECT GREATEST(0.0, (-0.033804) - (0.010468 * $1) + (0.001184 * $1 * $1)) AS "volume m3 tarif vaudois fonction";
$BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;""", "DROP FUNCTION ksp_vd_tarif_vaudois_fonction(diameter double precision)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION array_sum (smallint[]) returns int as $$
SELECT sum($1[i])::int FROM
generate_series(array_lower($1,1),array_upper($1,1)
) index(i); $$ language sql;""", "DROP FUNCTION array_sum(smallint[])"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_vd_ddom(plot_obs_id int)
  RETURNS integer AS
$BODY$
  SELECT round(array_sum(diams[:fact]) / least(fact, array_length(diams, 1)))::int AS ddom FROM (
    SELECT array(
        SELECT tree.diameter FROM tree WHERE tree.obs_id=plot_obs.id ORDER BY tree.diameter DESC
    ) AS diams,
        round(ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) / 100) AS fact
    FROM plot_obs
    WHERE id = plot_obs_id
  ) subq
$BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;""", "DROP FUNCTION ksp_vd_ddom(int)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION public.ksp_vd_tarif_vaudois_tableau(diameter smallint)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    IMMUTABLE
AS $BODY$
SELECT CASE
        WHEN $1 = 10 THEN 7
        WHEN $1 = 11 THEN 8
        WHEN $1 = 12 THEN 9
        WHEN $1 = 13 THEN 10
        WHEN $1 = 14 THEN 11
        WHEN $1 = 15 THEN 13
        WHEN $1 = 16 THEN 15
        WHEN $1 = 17 THEN 17
        WHEN $1 = 18 THEN 19
        WHEN $1 = 19 THEN 21
        WHEN $1 = 20 THEN 24
        WHEN $1 = 21 THEN 27
        WHEN $1 = 22 THEN 31
        WHEN $1 = 23 THEN 35
        WHEN $1 = 24 THEN 39
        WHEN $1 = 25 THEN 43
        WHEN $1 = 26 THEN 48
        WHEN $1 = 27 THEN 53
        WHEN $1 = 28 THEN 58
        WHEN $1 = 29 THEN 64
        WHEN $1 = 30 THEN 70
        WHEN $1 = 31 THEN 76
        WHEN $1 = 32 THEN 82
        WHEN $1 = 33 THEN 88
        WHEN $1 = 34 THEN 95
        WHEN $1 = 35 THEN 102
        WHEN $1 = 36 THEN 109
        WHEN $1 = 37 THEN 117
        WHEN $1 = 38 THEN 125
        WHEN $1 = 39 THEN 133
        WHEN $1 = 40 THEN 142
        WHEN $1 = 41 THEN 151
        WHEN $1 = 42 THEN 160
        WHEN $1 = 43 THEN 169
        WHEN $1 = 44 THEN 178
        WHEN $1 = 45 THEN 188
        WHEN $1 = 46 THEN 198
        WHEN $1 = 47 THEN 208
        WHEN $1 = 48 THEN 218
        WHEN $1 = 49 THEN 229
        WHEN $1 = 50 THEN 240
        WHEN $1 = 51 THEN 251
        WHEN $1 = 52 THEN 262
        WHEN $1 = 53 THEN 273
        WHEN $1 = 54 THEN 284
        WHEN $1 = 55 THEN 296
        WHEN $1 = 56 THEN 308
        WHEN $1 = 57 THEN 320
        WHEN $1 = 58 THEN 333
        WHEN $1 = 59 THEN 346
        WHEN $1 = 60 THEN 359
        WHEN $1 = 61 THEN 373
        WHEN $1 = 62 THEN 387
        WHEN $1 = 63 THEN 401
        WHEN $1 = 64 THEN 415
        WHEN $1 = 65 THEN 429
        WHEN $1 = 66 THEN 444
        WHEN $1 = 67 THEN 459
        WHEN $1 = 68 THEN 475
        WHEN $1 = 69 THEN 491
        WHEN $1 = 70 THEN 507
        WHEN $1 = 71 THEN 523
        WHEN $1 = 72 THEN 540
        WHEN $1 = 73 THEN 557
        WHEN $1 = 74 THEN 574
        WHEN $1 = 75 THEN 591
        WHEN $1 = 76 THEN 608
        WHEN $1 = 77 THEN 625
        WHEN $1 = 78 THEN 643
        WHEN $1 = 79 THEN 661
        WHEN $1 = 80 THEN 679
        WHEN $1 = 81 THEN 697
        WHEN $1 = 82 THEN 715
        WHEN $1 = 83 THEN 733
        WHEN $1 = 84 THEN 752
        WHEN $1 = 85 THEN 771
        WHEN $1 = 86 THEN 790
        WHEN $1 = 87 THEN 809
        WHEN $1 = 88 THEN 828
        WHEN $1 = 89 THEN 848
        WHEN $1 = 90 THEN 868
        WHEN $1 = 91 THEN 888
        WHEN $1 = 92 THEN 908
        WHEN $1 = 93 THEN 928
        WHEN $1 = 94 THEN 949
        WHEN $1 = 95 THEN 970
        WHEN $1 = 96 THEN 991
        WHEN $1 = 97 THEN 1012
        WHEN $1 = 98 THEN 1033
        WHEN $1 = 99 THEN 1055
        WHEN $1 = 100 THEN 1077
        WHEN $1 = 101 THEN 1099
        WHEN $1 = 102 THEN 1121
        WHEN $1 = 103 THEN 1143
        WHEN $1 = 104 THEN 1166
        WHEN $1 = 105 THEN 1189
        WHEN $1 = 106 THEN 1212
        WHEN $1 = 107 THEN 1236
        WHEN $1 = 108 THEN 1260
        WHEN $1 = 109 THEN 1284
        WHEN $1 = 110 THEN 1310
        WHEN $1 = 111 THEN 1337
        ELSE 0
    END AS "volume tarif vaudois tableau";
$BODY$;""",
            "DROP FUNCTION ksp_vd_tarif_vaudois_tableau"),
    ] + [
        # Loading views
        migrations.RunSQL(sql_code) for sql_code in read_sql(sql_dir)
    ]
