from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0035_plotobsplanned_owner_nullable'),
    ]

    operations = [
        migrations.CreateModel(
            name='PlotObsCalc',
            fields=[
                ('plot_obs', models.OneToOneField(db_column='id', on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='inventory.plotobs', verbose_name='Observation de placette')),
                ('surf_placette', models.FloatField(verbose_name='Surface de la placette')),
                ('year', models.SmallIntegerField(verbose_name='Année d’inventaire')),
                ('surface', models.IntegerField(verbose_name='Surface')),
                ('rpstz', models.FloatField(verbose_name='Surface représentative')),
                ('nb_tiges_10', models.SmallIntegerField(verbose_name='Nombre de tiges (diamètre >= 10)')),
                ('nb_tiges_16', models.SmallIntegerField(verbose_name='Nombre de tiges (diamètre >= 16)')),
                ('volume_tarif_vaudois_10', models.IntegerField(verbose_name='Volume de bois (m3, diamètre >= 10)')),
                ('volume_tarif_vaudois_16', models.IntegerField(verbose_name='Volume de bois (m3, diamètre >= 16)')),
                ('diametre_moyen_10', models.FloatField(blank=True, null=True, verbose_name='Diamètre moyen (cm, diamètre >= 10)')),
                ('diametre_moyen_16', models.FloatField(blank=True, null=True, verbose_name='Diamètre moyen (cm, diamètre >= 16)')),
                ('surf_terr_10', models.FloatField(verbose_name='Surface terrière (m2, diamètre >= 10)')),
                ('surf_terr_16', models.FloatField(verbose_name='Surface terrière (m2, diamètre >= 16)')),
                ('ddom', models.SmallIntegerField(blank=True, null=True, verbose_name='Diamètre dominant')),
                ('ddom_class', models.SmallIntegerField(blank=True, null=True, verbose_name='Classe de diamètre dominant')),
                ('g10', models.FloatField(verbose_name='g10')),
                ('g16', models.FloatField(verbose_name='g16')),
                ('surf_terr_moyen_10', models.FloatField(blank=True, null=True, verbose_name='Surface terrière moyenne (diamètre >= 10)')),
                ('surf_terr_moyen_16', models.FloatField(blank=True, null=True, verbose_name='Surface terrière moyenne (diamètre >= 16)')),
                ('diametre_moyen_pond_10', models.FloatField(blank=True, null=True, verbose_name='Diamètre moyen pondéré (diamètre >= 10)')),
                ('diametre_moyen_pond_16', models.FloatField(blank=True, null=True, verbose_name='Diamètre moyen pondéré (diamètre >= 16)')),
                ('owner', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='inventory.owner', verbose_name='Propriétaire')),
            ],
            options={
                'db_table': 'plot_obs_calculated',
            },
        ),
        migrations.CreateModel(
            name='PlotObsCalcView',
            fields=[
                ('plot_obs', models.OneToOneField(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='inventory.plotobs', verbose_name='Observation de placette')),
                ('surf_placette', models.FloatField(verbose_name='Surface de la placette')),
                ('year', models.SmallIntegerField(verbose_name='Année d’inventaire')),
                ('surface', models.IntegerField(verbose_name='Surface')),
                ('rpstz', models.FloatField(verbose_name='Surface représentative')),
                ('nb_tiges_10', models.SmallIntegerField(verbose_name='Nombre de tiges (diamètre >= 10)')),
                ('nb_tiges_16', models.SmallIntegerField(verbose_name='Nombre de tiges (diamètre >= 16)')),
                ('volume_tarif_vaudois_10', models.IntegerField(verbose_name='Volume de bois (m3, diamètre >= 10)')),
                ('volume_tarif_vaudois_16', models.IntegerField(verbose_name='Volume de bois (m3, diamètre >= 16)')),
                ('diametre_moyen_10', models.FloatField(blank=True, null=True, verbose_name='Diamètre moyen (cm, diamètre >= 10)')),
                ('diametre_moyen_16', models.FloatField(blank=True, null=True, verbose_name='Diamètre moyen (cm, diamètre >= 16)')),
                ('surf_terr_10', models.FloatField(verbose_name='Surface terrière (m2, diamètre >= 10)')),
                ('surf_terr_16', models.FloatField(verbose_name='Surface terrière (m2, diamètre >= 16)')),
                ('ddom', models.SmallIntegerField(blank=True, null=True, verbose_name='Diamètre dominant')),
                ('ddom_class', models.SmallIntegerField(blank=True, null=True, verbose_name='Classe de diamètre dominant')),
                ('g10', models.FloatField(verbose_name='g10')),
                ('g16', models.FloatField(verbose_name='g16')),
                ('surf_terr_moyen_10', models.FloatField(blank=True, null=True, verbose_name='Surface terrière moyenne (diamètre >= 10)')),
                ('surf_terr_moyen_16', models.FloatField(blank=True, null=True, verbose_name='Surface terrière moyenne (diamètre >= 16)')),
                ('diametre_moyen_pond_10', models.FloatField(blank=True, null=True, verbose_name='Diamètre moyen pondéré (diamètre >= 10)')),
                ('diametre_moyen_pond_16', models.FloatField(blank=True, null=True, verbose_name='Diamètre moyen pondéré (diamètre >= 16)')),
            ],
            options={
                'db_table': 'base_plotobs_calc',
                'abstract': False,
                'managed': False,
            },
        ),
    ]
