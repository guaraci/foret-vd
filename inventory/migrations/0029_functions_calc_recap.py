from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0028_plotobsplanned'),
    ]

    operations = [
        migrations.RunSQL("""
    CREATE OR REPLACE VIEW base_tree
    AS
    SELECT tree.id, tree.diameter, tree.spec_id, tree_spec.species, tree_spec.typ, tree.obs_id, plot_obs.owner_id,
           plot_obs.protocol_id, protocol.year, density.surface,
           density.surface / round(ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope)) AS repr_fact,
           ksp_vd_surface_terriere(tree.diameter::double precision) AS surface_terriere,
           ksp_vd_tarif_vaudois_tableau(diameter) / 100 AS volume,
           density.surface / round(ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope)) * ksp_vd_tarif_vaudois_tableau(diameter) / 100 AS volume_pond
        FROM tree
        LEFT JOIN tree_spec ON tree_spec.id = tree.spec_id
        LEFT JOIN plot_obs ON plot_obs.id = tree.obs_id
        LEFT JOIN protocol ON protocol.id = plot_obs.protocol_id
        LEFT JOIN density ON plot_obs.density_id = density.id;""",
            "DROP VIEW base_tree"),

        migrations.RunSQL("""
CREATE OR REPLACE VIEW public.base_surf_terr_species_gt10
 AS
 SELECT po_calc.obs_id,
    po_calc.spec_id,
    po_calc.species,
    10000::double precision / surf_placette * COALESCE(po_calc.surf_terr, 0::double precision) AS g10,
    prot.year,
    po.owner_id,
    ksp_vd_surface_terriere(COALESCE(po_calc.diametre_moyen, 0.0)::double precision) AS surf_terr_moyen,
    diametre_moyen,
    surf_placette,
    diametre_moyen * surf_placette AS diametre_moyen_pond
   FROM ( SELECT tree.obs_id,
            tree.spec_id,
            tree_spec.species,
            sqrt(avg(tree.diameter)*avg(tree.diameter)+var_pop(tree.diameter)) AS diametre_moyen,
            sum(ksp_vd_surface_terriere(tree.diameter)) AS surf_terr
           FROM tree
             LEFT JOIN tree_spec ON tree_spec.id = tree.spec_id
          WHERE tree.diameter > 9
          GROUP BY tree.obs_id, tree.spec_id, tree_spec.species) po_calc
     LEFT JOIN (SELECT id, owner_id, protocol_id, ksp_vd_surface_placette_exacte(radius, slope) AS surf_placette FROM plot_obs) po ON po_calc.obs_id = po.id
     LEFT JOIN protocol prot ON prot.id = po.protocol_id;""",
            "DROP VIEW base_surf_terr_species_gt10"),

        migrations.RunSQL("""
CREATE OR REPLACE VIEW base_volume_tiges_par_espece_classediam AS
    SELECT
        po_calc.id,
        trees.spec_id,
        trees.species,
        trees.diam_class,
        trees.volume,
        round(trees.volume * po_calc.repr_fact::numeric, 2) AS volume_real,
        trees.num_tiges,
        trees.num_tiges * po_calc.repr_fact::numeric AS num_tiges_real,
        prot.year,
        po_calc.owner_id
        FROM (
            SELECT plot_obs.id,
                plot_obs.protocol_id,
                plot_obs.owner_id,
                density.surface / round(ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope)) AS repr_fact
            FROM plot_obs
            LEFT JOIN density ON plot_obs.density_id = density.id
        ) po_calc
        LEFT JOIN (
            SELECT obs_id,
                spec_id,
                tree_spec.species,
                CASE WHEN diameter < 16 THEN 'N8-15'
                    WHEN diameter >= 16 AND diameter < 20 THEN 'N16-19'
                    WHEN diameter >= 20 AND diameter < 24 THEN 'N20-23'
                    WHEN diameter >= 24 AND diameter < 28 THEN 'N24-27'
                    WHEN diameter >= 28 AND diameter < 32 THEN 'N28-31'
                    WHEN diameter >= 32 AND diameter < 36 THEN 'N32-35'
                    WHEN diameter >= 36 AND diameter < 40 THEN 'N36-39'
                    WHEN diameter >= 40 AND diameter < 44 THEN 'N40-43'
                    WHEN diameter >= 44 AND diameter < 48 THEN 'N44-47'
                    WHEN diameter >= 48 AND diameter < 52 THEN 'N48-51'
                    WHEN diameter >= 52 AND diameter < 56 THEN 'N52-55'
                    WHEN diameter >= 56 AND diameter < 60 THEN 'N56-59'
                    WHEN diameter >= 60 AND diameter < 64 THEN 'N60-63'
                    WHEN diameter >= 64 AND diameter < 68 THEN 'N64-67'
                    WHEN diameter >= 68 AND diameter < 72 THEN 'N68-71'
                    WHEN diameter >= 72 AND diameter < 76 THEN 'N72-75'
                    WHEN diameter >= 76 AND diameter < 80 THEN 'N76-79'
                    ELSE 'N80+'
                END AS diam_class,
                sum(ksp_vd_tarif_vaudois_tableau(diameter)::decimal / 100) AS volume,
                count(*) AS num_tiges
                FROM tree
                LEFT JOIN tree_spec ON tree_spec.id = tree.spec_id
                GROUP BY obs_id, spec_id, species, diam_class
        ) trees ON po_calc.id = trees.obs_id
        LEFT JOIN protocol prot ON prot.id = po_calc.protocol_id;""",
        "DROP VIEW base_volume_tiges_par_espece_classediam"),
    ]
