# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0005_migrate_main_nature'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plotobs',
            name='main_nature',
        ),
        migrations.RenameField(
            model_name='plotobs',
            old_name='main_nature_id',
            new_name='main_nature',
        ),
        migrations.AlterField(
            model_name='plotobs',
            name='main_nature',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.PROTECT, to='inventory.Nature'),
            preserve_default=False,
        ),
    ]
