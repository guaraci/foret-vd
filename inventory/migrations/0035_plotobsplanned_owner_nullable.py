from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0034_plotobsplanned_operator'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plotobsplanned',
            name='owner',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='inventory.owner', verbose_name='Propriétaire'),
        ),
    ]
