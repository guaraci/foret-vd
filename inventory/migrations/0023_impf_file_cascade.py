from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('imports', '0007_imperror_context'),
        ('inventory', '0022_protocol_specs_0a9'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plotobs',
            name='imp_file',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.CASCADE, to='imports.FileImport'),
        ),
    ]
