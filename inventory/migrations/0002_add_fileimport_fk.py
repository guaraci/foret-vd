from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('inventory', '0001_initial'),
        ('imports', '0003_fileimport_year'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobs',
            name='imp_file',
            field=models.ForeignKey(to='imports.FileImport', null=True, on_delete=models.SET_NULL),
        ),
    ]
