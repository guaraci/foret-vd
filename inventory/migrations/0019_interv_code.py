from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0018_interv_species_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='intervention',
            name='code',
            field=models.CharField(blank=True, max_length=1, verbose_name='Code fbase'),
        ),
    ]
