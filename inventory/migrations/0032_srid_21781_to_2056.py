from django.contrib.gis.db.models.fields import MultiPolygonField, PointField
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0031_topap_model'),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(
            database_operations=[
                migrations.RunSQL("ALTER TABLE plot DROP CONSTRAINT IF EXISTS enforce_srid_center;"),
                migrations.RunSQL("""
        ALTER TABLE plot
         ALTER COLUMN center TYPE geometry(Point,2056)
          USING ST_Transform(center,2056);"""),
                migrations.RunSQL("ALTER TABLE plot ADD CONSTRAINT enforce_srid_center CHECK (st_srid(center) = 2056);"),
                migrations.RunSQL("""
        ALTER TABLE protocol
         ALTER COLUMN geom TYPE geometry(MultiPolygon,2056)
          USING ST_Transform(geom,2056);"""),
            ],
            state_operations=[
                migrations.AlterField(
                    model_name='plot',
                    name='center',
                    field=PointField(srid=2056, verbose_name='Coordonnées'),
                ),
                migrations.AlterField(
                    model_name='protocol',
                    name='geom',
                    field=MultiPolygonField(blank=True, null=True, srid=2056, verbose_name='Surface d’inventaire'),
                ),
            ]
        ),
    ]
