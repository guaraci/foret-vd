from django.db import migrations, models


def migrate_nature(apps, schema_editor):
    Nature = apps.get_model("inventory", "Nature")
    PlotObs = apps.get_model("inventory", "PlotObs")
    natures = []
    for i in range(10):
        nat = Nature.objects.create(code=i, description="nature %s" % i)
        PlotObs.objects.filter(main_nature=i).update(main_nature_id=nat)


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0004_add_main_nature_table'),
    ]

    operations = [
        migrations.RunPython(migrate_nature),
    ]
