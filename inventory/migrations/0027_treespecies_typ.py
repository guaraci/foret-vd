from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0026_add_indexes'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='typ',
            field=models.CharField(blank=True, choices=[('f', 'Feuillu'), ('r', 'Résineux')], max_length=1, verbose_name='Type'),
        ),
        migrations.AlterField(
            model_name='treespecies',
            name='abbrev',
            field=models.CharField(blank=True, max_length=4, verbose_name='Abréviation'),
        ),
        migrations.AlterField(
            model_name='treespecies',
            name='active',
            field=models.BooleanField(default=False, help_text='Indique si l’essence peut être saisie dans de nouveaux inventaires', verbose_name='Actif?'),
        ),
        migrations.AlterField(
            model_name='treespecies',
            name='species',
            field=models.CharField(max_length=100, verbose_name='Nom'),
        ),
    ]
