from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0023_impf_file_cascade'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plotobs',
            name='protocol',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='inventory.protocol'),
        ),
    ]
