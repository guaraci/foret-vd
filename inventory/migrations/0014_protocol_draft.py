from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0013_owner_name_length'),
    ]

    operations = [
        migrations.AddField(
            model_name='protocol',
            name='draft',
            field=models.BooleanField(default=False),
        ),
    ]
