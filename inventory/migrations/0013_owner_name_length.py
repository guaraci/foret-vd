from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0012_add_sealevel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='owner',
            name='name',
            field=models.CharField(blank=True, max_length=120),
        ),
        migrations.AlterField(
            model_name='owner',
            name='num',
            field=models.CharField(help_text='Position Ipasmofix', max_length=3),
        ),
    ]
