from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0020_treespecies_code_unique'),
    ]

    operations = [
        migrations.AddField(
            model_name='protocol',
            name='passed',
            field=models.BooleanField(default=True, help_text='L’inventaire est terminé.'),
        ),
    ]
