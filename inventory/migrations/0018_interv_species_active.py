from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0017_plotobs_ecorcage'),
    ]

    operations = [
        migrations.AddField(
            model_name='intervention',
            name='active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='treespecies',
            name='active',
            field=models.BooleanField(default=False),
        ),
    ]
