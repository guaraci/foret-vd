from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0038_plotobsplanned_topapdata'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseGibiAnalysis',
            fields=[
                ('plot_obs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='inventory.plotobs', verbose_name='Placette')),
                ('year', models.SmallIntegerField(verbose_name="Année d'inventaire")),
                ('gibi_epicea_ha', models.SmallIntegerField(verbose_name='Epicéas disponibles')),
                ('gibi_epicea_f_ha', models.SmallIntegerField(verbose_name='Epicéas frottés (%)')),
                ('gibi_epicea_a_ha', models.SmallIntegerField(verbose_name='Epicéas abroutis (%)')),
                ('gibi_conifer_ha', models.SmallIntegerField(verbose_name='Autres résineux disponibles')),
                ('gibi_conifer_f_ha', models.SmallIntegerField(verbose_name='Autres résineux frottés (%)')),
                ('gibi_conifer_a_ha', models.SmallIntegerField(verbose_name='Autres résineux abroutis (%)')),
                ('gibi_resineux_ha', models.SmallIntegerField(verbose_name='Résineux disponibles')),
                ('gibi_resineux_f_ha', models.SmallIntegerField(verbose_name='Résineux frottés (%)')),
                ('gibi_resineux_a_ha', models.SmallIntegerField(verbose_name='Résineux abroutis (%)')),
                ('gibi_leaved_ha', models.SmallIntegerField(verbose_name='Feuillus disponibles')),
                ('gibi_leaved_f_ha', models.SmallIntegerField(verbose_name='Feuillus frottés (%)')),
                ('gibi_leaved_a_ha', models.SmallIntegerField(verbose_name='Feuillus abroutis (%)')),
                ('gibi_total_ha', models.SmallIntegerField(verbose_name='Total disponibles')),
                ('gibi_total_f_ha', models.SmallIntegerField(verbose_name='Total frottés (%)')),
                ('gibi_total_a_ha', models.SmallIntegerField(verbose_name='Total abroutis (%)')),
            ],
            options={
                'db_table': 'base_gibi_analysis',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BaseSurfTerrSpeciesGT10',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('species', models.CharField(max_length=100, verbose_name='Nom essence')),
                ('g10', models.FloatField()),
                ('year', models.SmallIntegerField(verbose_name='Année')),
                ('surf_terr_moyen', models.FloatField()),
                ('diametre_moyen', models.FloatField(verbose_name='Diamètre moyen')),
                ('surf_placette', models.FloatField(verbose_name='Surface placette')),
                ('diametre_moyen_pond', models.FloatField(verbose_name='Diamètre moyen pondéré')),
            ],
            options={
                'db_table': 'base_surf_terr_species_gt10',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BaseTree',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('diameter', models.SmallIntegerField(verbose_name='Diamètre')),
                ('species', models.CharField(max_length=100, verbose_name='Nom essence')),
                ('typ', models.CharField(blank=True, choices=[('f', 'Feuillu'), ('r', 'Résineux')], max_length=1, verbose_name='Type essence')),
                ('year', models.SmallIntegerField(verbose_name='Année')),
                ('surface', models.IntegerField(verbose_name='Surface représentée en m2')),
                ('repr_fact', models.FloatField()),
                ('surface_terriere', models.FloatField()),
                ('volume', models.FloatField()),
                ('volume_pond', models.FloatField()),
            ],
            options={
                'db_table': 'base_tree',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BaseVolumeTigesClasseDiam',
            fields=[
                ('plot_obs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='inventory.plotobs', verbose_name='Placette')),
                ('species', models.CharField(max_length=100, verbose_name='Nom essence')),
                ('diam_class', models.CharField(max_length=6, verbose_name='Classe de diamètre')),
                ('volume', models.FloatField(verbose_name='Volume')),
                ('volume_real', models.FloatField()),
                ('num_tiges', models.IntegerField(verbose_name='Nombre de tiges')),
                ('num_tiges_real', models.FloatField()),
                ('year', models.SmallIntegerField(verbose_name='Année')),
            ],
            options={
                'db_table': 'base_volume_tiges_par_espece_classediam',
                'managed': False,
            },
        ),
    ]
