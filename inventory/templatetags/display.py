import json
import os
import sqlparse
from decimal import Decimal

from django import template
from django.contrib.gis.gdal import SpatialReference, CoordTransform
from django.utils import numberformat
from django.utils.html import format_html_join, mark_safe

register = template.Library()

ch1903 = SpatialReference(21781)
ch1903plus = SpatialReference(2056)
trans = CoordTransform(ch1903, ch1903plus)


@register.filter
def basename(ffield):
    return os.path.basename(ffield.name)


@register.filter(is_safe=True)
def strip_colon(label):
    return label.replace(":</label>", "</label>")


@register.filter
def getitem(dct, key):
    return dct[key]


@register.filter(is_safe=False)
def display_value(value):
    """Handle display of values with None or decimals."""
    if value is None:
        return ''
    elif isinstance(value, (float, Decimal)):
        return numberformat.format(round(value, 1), ',', decimal_pos=1, grouping=3, thousand_sep=' ')
    elif hasattr(value, 'coords'):
        return ' '.join([str(int(c)) for c in value.coords])
    return value


@register.filter
def instance_fields(inst):
    if not inst:
        return ''
    result = ''
    for field in inst._meta.get_fields():
        result += f'<div><span class="instance_field">{field.name}:</span> <span class="instance_value">{getattr(inst, field.name)}</span></div>'
    return mark_safe(f'<div>{result}</div>')


@register.filter
def stat_value(value, decimals=1):
    """"""
    if isinstance(value, (float, Decimal)):
        return round(value, decimals)
    return value


@register.simple_tag(takes_context=True)
def display_query_line(context, line):
    return format_html_join('', "<td>{}</td>", ((display_value(value),) for value in context['line_cleaner'](line)))


@register.filter
def display_sql(query):
    if isinstance(query, list):
        # Typically an aggregate result
        return '-'
    elif isinstance(query, str):
        sql = query
    else:
        sql = str(query.query)
    return sqlparse.format(sql, reindent=True, keyword_case='upper')


@register.filter
def swiss_rounded(coords):
    if coords:
        return '%s, %s' % (int(round(coords[0])), int(round(coords[1])))
    return '-'


@register.filter
def coords_to_js(geom):
    #if geom.srid != 2056:
    #    geom.transform(trans)
    return json.loads(geom.json)['coordinates']


@register.filter
def as_geojson(geom):
    #if geom.srid != 2056:
    #    geom.transform(trans)
    return ('{"type": "Feature", "geometry":%s,'
            ' "crs": { "type": "name", "properties": { "name": "urn:x-ogc:def:crs:EPSG:%s" } } }' % (geom.json, geom.srid))
