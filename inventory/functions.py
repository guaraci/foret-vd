from django.contrib.gis.db.models.functions import GeomOutputGeoFunc
from django.db.models import Aggregate
from django.db.models.expressions import Func


class ConvexHull(GeomOutputGeoFunc):
    arity = 1


class StdErrRel(Aggregate):
    """https://gitlab.com/guaraci/foret-vd/-/issues/111"""
    name = 'StdErrRel'
    template = 'stddev_samp(%(expressions)s) / NULLIF(sqrt(count(*))*2, 0) / NULLIF(avg(%(expressions)s), 0) * 100.0'


class TarifVD(Func):
    function = 'ksp_vd_tarif_vaudois_fonction'
    arity = 1


class SurfaceTerriere(Func):
    function = 'ksp_vd_surface_terriere'
    arity = 1
