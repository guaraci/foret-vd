var map = null;
var pointsLayer = null, clusterLayer = null;

L.Control.AddPoint = L.Control.extend({
    onAdd: (map) => {
        const div = L.DomUtil.create('div');
        div.textContent = '+ Plac.';
        div.title = 'Ajouter une placette';
        div.classList.add('add-point');
        div.id = 'btn-add-point';
        L.DomEvent
            .addListener(div, 'click', L.DomEvent.stopPropagation)
            .addListener(div, 'click', L.DomEvent.preventDefault)
            .addListener(div, 'click', onAddPointClick);
        return div;
    },
    onRemove: (map) => {}
});

L.control.addPoint = function(opts) {
    return new L.Control.AddPoint(opts);
}

L.Control.DelPoint = L.Control.extend({
    onAdd: (map) => {
        const div = L.DomUtil.create('div');
        div.textContent = '- Plac.';
        div.title = 'Supprimer une ou plusieurs placettes';
        div.classList.add('add-point');
        div.id = 'btn-del-point';
        L.DomEvent
            .addListener(div, 'click', L.DomEvent.stopPropagation)
            .addListener(div, 'click', L.DomEvent.preventDefault)
            .addListener(div, 'click', onDelPointClick);
        return div;
    },
    onRemove: (map) => {}
});

L.control.delPoint = function(opts) {
    return new L.Control.DelPoint(opts);
}

function getCSRFToken() {
    return document.cookie.split('; ').find(row => row.startsWith('csrftoken=')).split('=')[1];
}

function snapCoordsToGrid(coords) {
    coords = L.CRS.EPSG2056.project(coords);
    return L.CRS.EPSG2056.unproject({
        x: Math.round(coords.x / 100) * 100,
        y: Math.round(coords.y / 100) * 100
    });
}

function initPointLayers(url=null, fitBounds=true) {
    if (pointsLayer !== null) map.map.removeLayer(pointsLayer);
    if (clusterLayer !== null) map.map.removeLayer(clusterLayer);
    document.querySelector('#multi-edit-div').classList.add('hidden');
    document.querySelector('#plotobs_container').innerHTML = '';
    if (url === null) {
        url = document.getElementById('map').dataset.src;
    }
    let plotsDone = plotsVerified = plotsUnverified = plotsTotal = 0;
    let operators = {0: {name: 'Sans opérateur', count: 0}};
    map.addLayerFromURL(url, {
        clustered: true,
        clickHandler: onPlotClick,
        pointFunction: (feature, latlng) => {
            const serie = feature.properties.serie === null ? '-' : feature.properties.serie;
            const division = feature.properties.division === null ? '-' : feature.properties.division;
            let textContent = `<span class="owner hidden">${feature.properties.owner}</span>`;
            textContent += `<span class="ser_div hidden">${serie}${division}</span>`;
            let classes = ['custom-icon'];
            if (feature.properties.planned === false) {
                classes.push('icon-green');
                plotsDone += 1;
            }
            else {
                if (feature.properties.checked) {
                    classes.push('icon-blue');
                    plotsVerified += 1;
                } else {
                    classes.push('icon-unchecked');
                    plotsUnverified += 1;
                }
            }
            plotsTotal += 1;
            classes.push(`owner-${feature.properties.owner_id}`);
            if (feature.properties.operator_id) {
                classes.push(`operator-${feature.properties.operator_id}`);
                if (!(feature.properties.operator_id in operators)) {
                    operators[feature.properties.operator_id] = {
                        name: feature.properties.operator_name,
                        count: 0
                    };
                }
                operators[feature.properties.operator_id].count += 1;
            } else {
                operators[0].count += 1;
            }
            const myIcon = L.divIcon({
                iconSize: new L.Point(20, 20),
                className: classes.join(' '),
                html: textContent
            });
            return L.marker(latlng, {icon: myIcon});
        }
    }).then(lyrs => {
        if (fitBounds) map.map.fitBounds(lyrs[1].getBounds());
        pointsLayer = lyrs[0];
        clusterLayer = lyrs[1];
        document.querySelector('#plots-total').textContent = plotsTotal;
        document.querySelector('#plots-done').textContent = plotsDone;
        document.querySelector('#plots-verif').textContent = plotsVerified;
        document.querySelector('#plots-unverif').textContent = plotsUnverified;
        const operatorList = document.querySelector('#operators');
        operatorList.innerHTML = '';
        for (let key in operators) {
            let operator = operators[key];
            let li = document.createElement("li");
            li.innerHTML = `<input type="checkbox" id="cb-operator-${key}" class="cb-operator operator-${key}" data-operatorid="${key}"> ${operator.name} (${operator.count})`;
            operatorList.appendChild(li);
        }
        syncOwnerSerdivState();
    });
}

function onPlotClick(evt) {
    const plotobsContainer = document.querySelector('#plotobs_container');
    const targetURL = evt.layer.feature.properties.url;
    document.querySelector('#multi-edit-div').classList.add('hidden');
    map.selectPoints([evt.layer]);
    // A feature has been selected, let's display some details about it
    plotobsContainer.style.display = 'block';
    fetch(targetURL, {headers: {'X-Partial': 'true'}}).then(resp => resp.text()
        ).then(html => {
            plotobsContainer.innerHTML = html;
            htmx.process(plotobsContainer);
            // Now get parcelle info
            map.getFeatureInfo(evt, 'vd.bien_fond').then(data => {
                document.getElementById('parc-designation').textContent = data.features[0].properties["Désignation"];
                document.getElementById('parc-no').textContent = data.features[0].properties.N;
                const rf_url = data.features[0].properties["Lien RF"];
                document.getElementById('parcellaire').classList.remove('hidden');
                if (rf_url == "DP non disponible au registre foncier") {
                    document.getElementById('parcellaire-inconnu').classList.remove('hidden');
                } else {
                    document.getElementById('parcellaire-inconnu').classList.add('hidden');
                    // We cannot directly query www.rfinfo.vd.ch as the response has no CORS.
                    // http://www.rfinfo.vd.ch/rfinfo.php?no_commune=5871&amp;&no_immeuble=1390
                    let no_comm = /no_commune=(\d+)/.exec(rf_url)[1];
                    let no_imm = /no_immeuble=(\d+)/.exec(rf_url)[1];
                    fetch('/rfinfo/?no_commune=' + no_comm + '&no_immeuble=' + no_imm).then(
                        resp => resp.json()
                    ).then(json => {
                        document.getElementById('parc-owner').textContent = json.owners.join(', ');
                        document.getElementById('parc-genre').textContent = json.genre;
                    });
                }
            });
        });
}

function onAddPointClick(ev) {
    ev.preventDefault();
    const newCoords = snapCoordsToGrid(map.map.getCenter());
    const marker = L.marker(newCoords, {
        draggable: true,
        icon: L.icon({iconUrl: markerURL, iconSize: [25, 40], iconAnchor: [12.5, 40]})
    });
    marker.addTo(map.map);
    marker.on('dragend', ev => {
        // Snap marker to rounded coordinates to 100m.
        const marker = ev.target;
        const position = marker.getLatLng();
        const newCoords = snapCoordsToGrid(position)
        marker.setLatLng(newCoords);
        const swiss = map.crs.project(newCoords);
        document.getElementById('newp-latitude').innerText = Math.round(swiss.x);
        document.getElementById('newp-longitude').innerText = Math.round(swiss.y);
    });
    document.querySelector('#plotobs_container').innerHTML = (
        '<div id="planned-details" class="flex-child">' +
        '<p></p><b>Coordonnées:</b> <span id="newp-latitude"></span>, <span id="newp-longitude"></span></p>' +
        '<button id="btn-confirm-point">Confirmer le nouveau point</button>' +
        '<button id="btn-cancel-point">Annuler le nouveau point</button></div>'
    );
    const swiss = map.crs.project(newCoords);
    document.getElementById('newp-latitude').innerText = Math.round(swiss.x);
    document.getElementById('newp-longitude').innerText = Math.round(swiss.y);
    document.getElementById('btn-confirm-point').addEventListener('click', ev => {
        const url = document.getElementById('map').dataset.addpoint;
        const formData = new FormData();
        formData.append('latitude', document.getElementById('newp-latitude').innerText);
        formData.append('longitude', document.getElementById('newp-longitude').innerText);
        // Send new point to server
        fetch(url, {
                headers: {'X-CSRFToken': getCSRFToken()}, method: 'POST', body: formData
            }).then(resp => resp.json()).then(data => {
            let position = marker.getLatLng();
            map.map.removeLayer(marker);
            // Add the new point to the layer as a "regular" point
            pointsLayer.addData([data.feature]);
            // clusterLayer doesn't link to the initial geoJSON layer, re-create it after
            // adding a new point.
            clusterLayer.clearLayers();
            clusterLayer.addLayer(pointsLayer);
        });
    });
    document.getElementById('btn-cancel-point').addEventListener('click', ev => {
        ev.preventDefault();
        document.getElementById('plotobs_container').innerHTML = '';
        map.map.removeLayer(marker);
    });
}

function onDelPointClick(ev) {
    if (!(map.selectedPoints.length)) { alert("Aucun point sélectionné."); return; }
    let msg = "Voulez-vous vraiment retirer ce point de l’inventaire ?";
    if (map.selectedPoints.length > 1) msg = `Voulez-vous vraiment retirer ${map.selectedPoints.length} points de l’inventaire ?`;
    if (confirm(msg)) {
        ev.preventDefault();
        // Send info to server
        const curLayer = map.selectedPoints[0];
        const removeURL = document.getElementById('map').dataset.delpoint;
        const formData = new FormData();
        formData.append('point_ids', map.selectedPoints.map(pt => pt.feature.id).join(','));
        fetch(removeURL, {
                headers: {'X-Partial': 'true', 'X-CSRFToken': getCSRFToken()},
                method: 'POST',
                body: formData
            }).then(resp => {
            // Remove marker from the map
            map.selectedPoints.forEach(pt => {
                map.map.removeLayer(pt);
                pointsLayer.removeLayer(pt);
            })
            map.selectedPoints = [];
            document.querySelector('#multi-edit-div').classList.add('hidden');
            document.querySelector('#plotobs_container').innerHTML = '';
        });
    }
}

function onPlotCheckboxClick(ev) {
    if (ev.target.tagName != 'INPUT') return;
    Array.from(document.querySelectorAll('.cb-operator')).forEach(cb => cb.checked = false);
    Array.from(document.querySelectorAll('.cb-owner')).forEach(cb => cb.checked = false);
    // Get all checked checkbox
    let cbs = Array.from(document.querySelectorAll('.cb-plot'));
    cbs = cbs.filter(cb => cb.checked == true);
    if (cbs.length == 0) {
        // show all points
        map.unfilterPoints(clusterLayer);
    } else {
        map.filterPoints(clusterLayer, (lyr) => {
            return cbs.some(cb => {
                if (cb.id == 'cb-plots-done') return lyr.feature.properties.planned === false;
                else if (cb.id == 'cb-plots-verif') return lyr.feature.properties.planned && lyr.feature.properties.checked === true;
                else if (cb.id == 'cb-plots-unverif') return lyr.feature.properties.planned && lyr.feature.properties.checked === false;
            });
        });
    }
}

function onOwnerCheckboxClick(ev) {
    Array.from(document.querySelectorAll('.cb-operator')).forEach(cb => cb.checked = false);
    Array.from(document.querySelectorAll('.cb-plot')).forEach(cb => cb.checked = false);
    // Get all checked checkbox
    let cbs = Array.from(document.querySelectorAll('.cb-owner'));
    cbs = cbs.filter(cb => cb.checked == true);
    if (cbs.length == 0) {
        // show all points
        map.unfilterPoints(clusterLayer);
    } else {
        map.filterPoints(clusterLayer, (lyr) => {
            return cbs.some(cb => lyr.feature.properties.owner_id == cb.dataset.ownerid);
        });
    }
}

function onOperatorCheckboxClick(ev) {
    if (ev.target.tagName != 'INPUT') return;
    Array.from(document.querySelectorAll('.cb-owner')).forEach(cb => cb.checked = false);
    Array.from(document.querySelectorAll('.cb-plot')).forEach(cb => cb.checked = false);
    let cbs = Array.from(document.querySelectorAll('.cb-operator'));
    cbs = cbs.filter(cb => cb.checked == true);
    if (cbs.length == 0) {
        // show all points
        map.unfilterPoints(clusterLayer);
    } else {
        map.filterPoints(clusterLayer, (lyr) => {
            return cbs.some(cb => (lyr.feature.properties.operator_id || 0) == cb.dataset.operatorid);
        });
    }
}

function onDiffAMEClick(ev) {
    if (this.checked) {
        initPointLayers(document.getElementById('map').dataset.srcconflicts);
    } else {
        initPointLayers();
    }
}

var onMultiSelected = function() {
    const plotobsContainer = document.querySelector('#plotobs_container');
    plotobsContainer.innerHTML = `${map.selectedPoints.length} points sélectionnés.`;
    document.querySelector('#multi-edit-div').classList.remove('hidden');
    document.querySelector('#multi-edit').reset();
    document.querySelectorAll('input[name="point_ids"]').forEach(input => {
        input.value = map.selectedPoints.filter(pt => pt.feature !== undefined).map(pt => pt.feature.id).join(',');
    });
}

function syncOwnerSerdivState() {
    const ownerCB = document.getElementById('disp-owner');
    const serdivCB = document.getElementById('disp-ser_div');
    document.querySelectorAll('span.owner').forEach(span => {
        if (ownerCB.checked) span.classList.remove('hidden');
        else span.classList.add('hidden');
    });
    document.querySelectorAll('span.ser_div').forEach(span => {
        if (serdivCB.checked) span.classList.remove('hidden');
        else span.classList.add('hidden');
    });
}

document.addEventListener("DOMContentLoaded", (event) => {
    map = new GeoMap('map', {withLasso: true, lassoSelected: onMultiSelected, gridLayer: true});
    initPointLayers();
    // Add Topap layer
    topapLayer = map.addLayerFromURL(
        document.getElementById('map').dataset.topap,
        {initial: 'hidden', style: {"color": "#ff7800", "weight": 2, "opacity": 0.65}}
    ).then(layer => {
        map.layerControl.addOverlay(layer, 'Topap');
        layer.eachLayer((lay) => {
            const props = lay.feature.properties;
            lay.bindPopup(`${props.triage}, P: ${props.owner}<br>S: ${props.serie}, D: ${props.division}`);
        });
    });
    L.control.addPoint({ position: 'topright' }).addTo(map.map);
    L.control.delPoint({ position: 'topright' }).addTo(map.map);

    const ownerCB = document.getElementById('disp-owner');
    ownerCB.addEventListener('click', (ev) => {
        ev.stopPropagation();
        document.querySelectorAll('span.owner').forEach(span => {
            if (ev.target.checked) span.classList.remove('hidden');
            else span.classList.add('hidden');
        });
    });
    const serdivCB = document.getElementById('disp-ser_div');
    serdivCB.addEventListener('click', (ev) => {
        ev.stopPropagation();
        document.querySelectorAll('span.ser_div').forEach(span => {
            if (ev.target.checked) span.classList.remove('hidden');
            else span.classList.add('hidden');
        });
    });
    map.map.on('zoomend', ev => {
        // setTimeout because I don't know how to catch end of unclustering event.
        setTimeout(() => { syncOwnerSerdivState(); }, 500);
    });

    document.querySelectorAll('.cb-owner').forEach(cb => {
        cb.addEventListener('click', onOwnerCheckboxClick);
    });
    document.querySelectorAll('.cb-plot').forEach(cb => {
        cb.addEventListener('click', onPlotCheckboxClick);
    });
    document.querySelector('#operators').addEventListener('click', onOperatorCheckboxClick);
    document.querySelector('#cb-diffametopap').addEventListener('change', onDiffAMEClick);
    // Event triggered by htmx HX-Trigger header
    document.body.addEventListener("PointModified", (ev) => {
        initPointLayers(null, false);
    });
    document.body.addEventListener('htmx:afterSwap', (evt) => {
        $('#id_owner').select2();
    });
});
