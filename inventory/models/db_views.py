"""Django models for Database views."""

from django.db import models
from django.urls import reverse

from .models import (
    Intervention, Municipality, Nature, Owner, Plot, PlotObs, Protocol,
    SpeciesGroup, TreeSpecies, Vocabulary
)


class PlotObsCalcBase(models.Model):
    plot_obs = models.OneToOneField(
        PlotObs, verbose_name="Observation de placette", on_delete=models.DO_NOTHING,
        db_column='id', primary_key=True
    )
    owner = models.ForeignKey(Owner, verbose_name="Propriétaire", null=True, on_delete=models.DO_NOTHING)
    surf_placette = models.FloatField("Surface de la placette")
    year = models.SmallIntegerField("Année d’inventaire")
    surface = models.IntegerField("Surface")
    rpstz = models.FloatField("Surface représentative")
    nb_tiges_10 = models.SmallIntegerField("Nombre de tiges (diamètre >= 10)")
    nb_tiges_16 = models.SmallIntegerField("Nombre de tiges (diamètre >= 16)")
    volume_tarif_vaudois_10 = models.IntegerField("Volume de bois (m3, diamètre >= 10)")
    volume_tarif_vaudois_16 = models.IntegerField("Volume de bois (m3, diamètre >= 16)")
    diametre_moyen_10 = models.FloatField("Diamètre moyen (cm, diamètre >= 10)", null=True, blank=True)
    diametre_moyen_16 = models.FloatField("Diamètre moyen (cm, diamètre >= 16)", null=True, blank=True)
    surf_terr_10 = models.FloatField("Surface terrière (m2, diamètre >= 10)")
    surf_terr_16 = models.FloatField("Surface terrière (m2, diamètre >= 16)")
    ddom = models.SmallIntegerField("Diamètre dominant", null=True, blank=True)
    ddom_class = models.SmallIntegerField("Classe de diamètre dominant", null=True, blank=True)
    g10 = models.FloatField("g10")
    g16 = models.FloatField("g16")
    surf_terr_moyen_10 = models.FloatField("Surface terrière moyenne (diamètre >= 10)", null=True, blank=True)
    surf_terr_moyen_16 = models.FloatField("Surface terrière moyenne (diamètre >= 16)", null=True, blank=True)
    diametre_moyen_pond_10 = models.FloatField("Diamètre moyen pondéré (diamètre >= 10)", null=True, blank=True)
    diametre_moyen_pond_16 = models.FloatField("Diamètre moyen pondéré (diamètre >= 16)", null=True, blank=True)

    class Meta:
        abstract = True


class PlotObsCalc(PlotObsCalcBase):
    """
    This is a real table, but updated only through the view base_plotobs_calc.
    It is possible to manually update the whole table by:
    BEGIN;
    DELETE FROM plot_obs_calculated;
    INSERT INTO plot_obs_calculated (
        id, owner_id, surf_placette, year, surface, rpstz, nb_tiges_10, nb_tiges_16, volume_tarif_vaudois_10,
        volume_tarif_vaudois_16, diametre_moyen_10, diametre_moyen_16, surf_terr_10, surf_terr_16, ddom,
        ddom_class, g10, g16, surf_terr_moyen_10, surf_terr_moyen_16, diametre_moyen_pond_10,
        diametre_moyen_pond_16
    ) SELECT * FROM base_plotobs_calc;
    COMMIT;
    """
    plot_obs = models.OneToOneField(
        PlotObs, verbose_name="Observation de placette", on_delete=models.CASCADE,
        db_column='id', primary_key=True
    )
    owner = models.ForeignKey(Owner, verbose_name="Propriétaire", null=True, on_delete=models.CASCADE)

    class Meta:
        db_table = 'plot_obs_calculated'
        abstract = False


class PlotObsCalcView(PlotObsCalcBase):
    """This is the view to update plot_obs_calculated."""

    class Meta:
        db_table = 'base_plotobs_calc'
        abstract = False
        managed = False


class BaseDbViewDiametre(models.Model):
    plot_obs = models.OneToOneField(PlotObs, verbose_name="Observation de placette", on_delete=models.DO_NOTHING,
        db_column='id', primary_key=True)
    plot = models.ForeignKey(Plot, verbose_name="Placette", null=True, on_delete=models.DO_NOTHING)
    municipality = models.ForeignKey(Municipality, verbose_name="Commune", null=True, on_delete=models.DO_NOTHING)
    interv_int = models.ForeignKey(Intervention, verbose_name="Intervention", null=True, on_delete=models.DO_NOTHING)
    main_nature = models.ForeignKey(Nature, verbose_name="Nature principale", on_delete=models.DO_NOTHING)
    owner = models.ForeignKey(Owner, verbose_name="Propriétaire", null=True, on_delete=models.DO_NOTHING)
    protocol = models.ForeignKey(Protocol, verbose_name="Protocole", null=True, on_delete=models.DO_NOTHING)
    # Field only used when grouping by species group
    group = models.ForeignKey(SpeciesGroup, verbose_name="Groupe d’essences", on_delete=models.DO_NOTHING)
    nb_tree = models.IntegerField("Nombre de tiges", db_column="nombre de tiges")
    rpstz = models.FloatField(db_column="RPSTZ")
    nb_tree_ha = models.FloatField("Nombre de tiges [N/ha]", db_column="nombre de tiges/hectare")
    volume_ha = models.FloatField("Volume sur pied [m3/ha]", db_column="volume (m3) tarif vaudois/hectare")
    surface_ha = models.FloatField("Surface terrière [m2/ha]", db_column="surface terriere (m2)/hectare")
    diam_avg = models.FloatField("Diamètre moyen", db_column="diametre moyen")
    avg_tree_volume = models.FloatField("Volume de l'arbre moyen", db_column="volume de l'arbre moyen")
    avg_tree_surface = models.FloatField("Surface terrière de l'arbre moyen", db_column="surface terriere de l'arbre moyen")
    surface_f = models.IntegerField("Surface forestière [ha]", db_column="surface_forestiere_ha")

    class Meta:
        managed = False
        abstract=True


class BaseDbViewDiametre16(BaseDbViewDiametre):
    class Meta:
        db_table = 'base_surf_vol_par_plotobs_diametre_16'
        managed = False


class BaseDbViewDiametre9(BaseDbViewDiametre):
    class Meta:
        db_table = 'base_surf_vol_par_plotobs_diametre_9'
        managed = False


class WebHomepageOwner(models.Model):
    protocol = models.ForeignKey(Protocol, verbose_name="Protocole", null=True, on_delete=models.DO_NOTHING)
    owner = models.ForeignKey(Owner, verbose_name="Propriétaire", null=True, on_delete=models.DO_NOTHING)
    plot_numb = models.IntegerField("Nombre de placettes", db_column="nombre de placettes")
    tree_numb_ha = models.FloatField("Nombre de tiges (>= 16cm)/hectare",
        db_column="nombre de tiges (>= 16cm)/hectare")
    vol_vd_ha = models.FloatField("Volume (m3) tarif vaudois/hectare",
        db_column="volume (m3) tarif vaudois/hectare")
    surf_terr_ha = models.FloatField("Surface terriere (m2)/hectare",
        db_column="surface terriere (m2)/hectare")
    diam_mean = models.FloatField("Diametre moyen (cm2)",
        db_column="diametre moyen (cm2)")
    vol_mean = models.FloatField("Volume de l'arbre moyen (m3)",
        db_column="volume de l'arbre moyen (m3)")
    surf_terr_tree_mean = models.FloatField("Surface terriere de l'arbre moyen (cm2)",
        db_column="surface terriere de l'arbre moyen (cm2)")
    surf_forest = models.FloatField("Surface forestiere theorique",
        db_column="surface forestiere theorique")

    class Meta:
        db_table = 'web_homepage_owner_diametre_16'
        managed = False

    def entity(self):
        return self.owner

    def entity_url(self):
        return reverse('entity', args=['owner', self.owner_id])


class WebHomepageMunicipality(models.Model):
    protocol = models.ForeignKey(Protocol, verbose_name="Protocole", null=True, on_delete=models.DO_NOTHING)
    municipality = models.ForeignKey(Municipality, verbose_name="Commune", null=True, on_delete=models.DO_NOTHING)
    plot_numb = models.IntegerField("Nombre de placettes", db_column="nombre de placettes")
    tree_numb_ha = models.FloatField("Nombre de tiges (>= 16cm)/hectare",
        db_column="nombre de tiges (>= 16cm)/hectare")
    vol_vd_ha = models.FloatField("Volume (m3) tarif vaudois/hectare",
        db_column="volume (m3) tarif vaudois/hectare")
    surf_terr_ha = models.FloatField("Surface terriere (m2)/hectare",
        db_column="surface terriere (m2)/hectare")
    diam_mean = models.FloatField("Diametre moyen (cm2)",
        db_column="diametre moyen (cm2)")
    vol_mean = models.FloatField("Volume de l'arbre moyen (m3)",
        db_column="volume de l'arbre moyen (m3)")
    surf_terr_tree_mean = models.FloatField("Surface terriere de l'arbre moyen (cm2)",
        db_column="surface terriere de l'arbre moyen (cm2)")
    surf_forest = models.FloatField("Surface forestiere theorique",
        db_column="surface forestiere theorique")

    class Meta:
        db_table = 'web_homepage_municipality_diametre_16'
        managed = False

    def entity(self):
        return self.municipality

    def entity_url(self):
        if self.municipality_id:
            return reverse('entity', args=['municipality', self.municipality_id])
        else:
            return ''


class BaseTree(models.Model):
    diameter = models.SmallIntegerField("Diamètre")
    spec = models.ForeignKey(TreeSpecies, on_delete=models.PROTECT, verbose_name="Essence")
    species = models.CharField("Nom essence", max_length=100)
    typ = models.CharField("Type essence", max_length=1, choices=TreeSpecies.TYP_CHOICES, blank=True)
    plot_obs = models.ForeignKey(PlotObs, db_column='obs_id', verbose_name="Placette", on_delete=models.DO_NOTHING)
    owner = models.ForeignKey(Owner, verbose_name="Propriétaire", on_delete=models.DO_NOTHING)
    protocol = models.ForeignKey(Protocol, verbose_name="Protocole", null=True, on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField("Année")
    surface = models.IntegerField("Surface représentée en m2")
    repr_fact = models.FloatField()
    surface_terriere = models.FloatField()
    volume = models.FloatField()
    volume_pond = models.FloatField()

    class Meta:
        db_table = 'base_tree'
        managed = False


class BaseVolumeTigesClasseDiam(models.Model):
    plot_obs = models.ForeignKey(PlotObs, db_column='id', verbose_name="Placette", primary_key=True, on_delete=models.DO_NOTHING)
    spec = models.ForeignKey(TreeSpecies, verbose_name="Essence", on_delete=models.DO_NOTHING)
    species = models.CharField("Nom essence", max_length=100)
    diam_class = models.CharField("Classe de diamètre", max_length=6)
    volume = models.FloatField("Volume")
    volume_real = models.FloatField()
    num_tiges = models.IntegerField("Nombre de tiges")
    num_tiges_real = models.FloatField()
    year = models.SmallIntegerField("Année")
    owner = models.ForeignKey(Owner, verbose_name="Propriétaire", on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'base_volume_tiges_par_espece_classediam'
        managed = False


class BaseSurfTerrSpeciesGT10(models.Model):
    plot_obs = models.ForeignKey(PlotObs, db_column='obs_id', verbose_name="Placette", on_delete=models.DO_NOTHING)
    spec = models.ForeignKey(TreeSpecies, verbose_name="Essence", on_delete=models.DO_NOTHING)
    species = models.CharField("Nom essence", max_length=100)
    g10 = models.FloatField()
    year = models.SmallIntegerField("Année")
    owner = models.ForeignKey(Owner, verbose_name="Propriétaire", on_delete=models.DO_NOTHING)
    surf_terr_moyen = models.FloatField()
    diametre_moyen = models.FloatField("Diamètre moyen")
    surf_placette = models.FloatField("Surface placette")
    diametre_moyen_pond = models.FloatField("Diamètre moyen pondéré")

    class Meta:
        db_table = 'base_surf_terr_species_gt10'
        managed = False


class BaseGibiAnalysis(models.Model):
    plot_obs = models.ForeignKey(PlotObs, db_column='id', on_delete=models.DO_NOTHING, primary_key=True, verbose_name="Placette")
    year = models.SmallIntegerField("Année d'inventaire")
    owner = models.ForeignKey(Owner, verbose_name="Propriétaire", null=True, on_delete=models.DO_NOTHING)
    gibi_epicea_ha = models.SmallIntegerField("Epicéas disponibles")
    gibi_epicea_f_ha = models.SmallIntegerField("Epicéas frottés (%)")
    gibi_epicea_a_ha = models.SmallIntegerField("Epicéas abroutis (%)")
    gibi_conifer_ha = models.SmallIntegerField("Autres résineux disponibles")
    gibi_conifer_f_ha = models.SmallIntegerField("Autres résineux frottés (%)")
    gibi_conifer_a_ha = models.SmallIntegerField("Autres résineux abroutis (%)")
    gibi_resineux_ha = models.SmallIntegerField("Résineux disponibles")
    gibi_resineux_f_ha = models.SmallIntegerField("Résineux frottés (%)")
    gibi_resineux_a_ha = models.SmallIntegerField("Résineux abroutis (%)")
    gibi_leaved_ha = models.SmallIntegerField("Feuillus disponibles")
    gibi_leaved_f_ha = models.SmallIntegerField("Feuillus frottés (%)")
    gibi_leaved_a_ha = models.SmallIntegerField("Feuillus abroutis (%)")
    gibi_total_ha = models.SmallIntegerField("Total disponibles")
    gibi_total_f_ha = models.SmallIntegerField("Total frottés (%)")
    gibi_total_a_ha = models.SmallIntegerField("Total abroutis (%)")

    class Meta:
        db_table = 'base_gibi_analysis'
        managed = False


class VocabularyValue(models.Model):
    plot_obs = models.ForeignKey(PlotObs, verbose_name="Observation de placette", on_delete=models.DO_NOTHING)
    vocab = models.ForeignKey(Vocabulary, verbose_name="Vocabulaire", on_delete=models.DO_NOTHING)
    value = models.CharField("Valeur", max_length=1)

    class Meta:
        db_table = 'base_vocabulary_values'
        managed = False


class AME_CE(models.Model):
    class Meta:
        db_table = 'imports\".\"ame_ce'

    id = models.IntegerField(primary_key=True, db_column="OBJECTID")
    # This field matches the 'N<id>' field of recent inventories.
    inv_GuidInv = models.IntegerField(null=True, db_index=True)
    CoordX = models.IntegerField(null=True)
    CoordY = models.IntegerField(null=True)
    inv_FkCxCy = models.CharField(max_length=15)
    # Champs META
    DateCreation = models.TextField(null=True)
    DateModif = models.TextField(null=True)
    MachineCreation = models.TextField(null=True)
    MachineModif = models.TextField(null=True)
    AuteurCreation = models.TextField(null=True)
    AuteurModif = models.TextField(null=True)
    BaseSource = models.TextField(null=True)
    TableSource = models.TextField(null=True)
    RespFiche = models.TextField(null=True)
    RespPrecFiche = models.FloatField(null=True)

    inv_MpmgFeuillus = models.IntegerField(null=True)
    inv_MpmgResineux = models.IntegerField(null=True)
    inv_MpmgTotal = models.IntegerField(null=True)
    inv_GpmgFeuillus = models.IntegerField(null=True)
    inv_GpmgResineux = models.IntegerField(null=True)
    inv_GpmgTotal = models.IntegerField(null=True)
    inv_PpmgFeuillus = models.IntegerField(null=True)
    inv_PpmgResineux = models.IntegerField(null=True)
    inv_PpmgTotal = models.IntegerField(null=True)
    inv_EpiceaAbro = models.FloatField(null=True)
    inv_EpiceaFrot = models.FloatField(null=True)
    inv_FeuillusAbro = models.FloatField(null=True)
    inv_FeuillusFrot = models.FloatField(null=True)
    inv_NbTigeFeuPC = models.IntegerField(null=True)
    inv_NbTigeResPC = models.IntegerField(null=True)
    inv_ResineuxAbro = models.FloatField(null=True)
    inv_ResineuxFrot = models.FloatField(null=True)
    inv_GFeuPC = models.IntegerField(null=True)
    inv_GResPC = models.IntegerField(null=True)
    inv_VoChenePC = models.FloatField(null=True)
    inv_VoDouglasPC = models.FloatField(null=True)
    inv_VoEpiceaPC = models.FloatField(null=True)
    inv_VoErablePC = models.FloatField(null=True)
    inv_VoFeuPC = models.FloatField(null=True)
    inv_VoFeuillus1PC = models.FloatField(null=True)
    inv_VoFeuillus2PC = models.FloatField(null=True)
    inv_VoFeuDivPC = models.FloatField(null=True)
    inv_VoFrenePC = models.FloatField(null=True)
    inv_VoHetrePC = models.FloatField(null=True)
    inv_VoMelezePC = models.FloatField(null=True)
    inv_VoMerisierPC = models.FloatField(null=True)
    inv_VoPeuplierPC = models.FloatField(null=True)
    inv_VoPinPC = models.FloatField(null=True)
    inv_VoResPC = models.FloatField(null=True)
    inv_VoResDivPC = models.FloatField(null=True)
    inv_VoSapinPC = models.FloatField(null=True)
    inv_InvAnnee = models.IntegerField(null=True)
    inv_ChampLib1 = models.TextField(null=True)
    inv_ChampLib2 = models.TextField(null=True)
    inv_ChampLib3 = models.TextField(null=True)
    inv_CodeStratification = models.FloatField(null=True)
    inv_LvDernierInv = models.TextField(null=True)
    inv_DiDom = models.IntegerField(null=True)
    inv_DiMoFeuillus = models.IntegerField(null=True)
    inv_DiMoResineux = models.IntegerField(null=True)
    inv_DiMo = models.IntegerField(null=True)
    inv_Fbase = models.TextField(null=True)
    inv_LvGestionPassee = models.FloatField(null=True)
    inv_Interv = models.TextField(null=True)
    inv_MelMoins10 = models.TextField(null=True)
    inv_MelPlus16 = models.TextField(null=True)
    inv_Mel10a16 = models.TextField(null=True)
    inv_LvNatureInv = models.IntegerField(null=True)
    inv_NbResDispo = models.FloatField(null=True)
    inv_NbTigeChene16P = models.IntegerField(null=True)
    inv_NbTigeChene1016 = models.IntegerField(null=True)
    inv_NbTigeDouglas16P = models.IntegerField(null=True)
    inv_NbTigeDouglas1016 = models.IntegerField(null=True)
    inv_NbTigeFeu10 = models.IntegerField(null=True)
    inv_NbTigeFeu16P = models.IntegerField(null=True)
    inv_NbTigeFeuillus116P = models.IntegerField(null=True)
    inv_NbTigeFeuillus11016 = models.IntegerField(null=True)
    inv_NbTigeFeuillus216P = models.IntegerField(null=True)
    inv_NbTigeFeuillus21016 = models.IntegerField(null=True)
    inv_NbTigeFeu1016 = models.IntegerField(null=True)
    inv_NbTigeFeuDiv16P = models.IntegerField(null=True)
    inv_NbTigeFeuDiv1016 = models.IntegerField(null=True)
    inv_NbTigeFrene16P = models.IntegerField(null=True)
    inv_NbTigeFrene1016 = models.IntegerField(null=True)
    inv_NbTigeHetre16P = models.IntegerField(null=True)
    inv_NbTigeHetre1016 = models.IntegerField(null=True)
    inv_NbTigeMeleze16P = models.IntegerField(null=True)
    inv_NbTigeMeleze1016 = models.IntegerField(null=True)
    inv_NbTigeMerisier16P = models.IntegerField(null=True)
    inv_NbTigeMerisier1016 = models.IntegerField(null=True)
    inv_NbTigePeuplier16P = models.IntegerField(null=True)
    inv_NbTigePeuplier1016 = models.IntegerField(null=True)
    inv_NbTigePin16P = models.IntegerField(null=True)
    inv_NbTigePin1016 = models.IntegerField(null=True)
    inv_NbTigeRes10 = models.IntegerField(null=True)
    inv_NbTigeRes16P = models.IntegerField(null=True)
    inv_NbTigeRes1016 = models.IntegerField(null=True)
    inv_NbTigeResDiv16P = models.IntegerField(null=True)
    inv_NbTigeResDiv1016 = models.IntegerField(null=True)
    inv_NbTigeSapin16P = models.IntegerField(null=True)
    inv_NbTigeSapin1016 = models.IntegerField(null=True)
    inv_NbTigeEpicea16P = models.IntegerField(null=True)
    inv_NbTigeEpicea1016 = models.IntegerField(null=True)
    inv_NbTigeErable16P = models.IntegerField(null=True)
    inv_NbTigeErable1016 = models.IntegerField(null=True)
    inv_NbTige10 = models.IntegerField(null=True)
    inv_NbTige16P = models.IntegerField(null=True)
    inv_NbTige1016 = models.IntegerField(null=True)
    inv_NbEpiceaDispo = models.FloatField(null=True)
    inv_NbFeuillusDispo = models.FloatField(null=True)
    inv_PenteInv = models.IntegerField(null=True)
    Lv_PropriHist = models.TextField(null=True)
    inv_Protocole = models.TextField(null=True)
    inv_protocoleLiaison = models.TextField(null=True)
    inv_Region = models.TextField(null=True)
    inv_RemInv = models.TextField(null=True)
    inv_RemCodeeR1 = models.FloatField(null=True)
    inv_RemCodeeR2 = models.FloatField(null=True)
    inv_SurfaceRepres = models.IntegerField(null=True)
    inv_Gfeuillus = models.IntegerField(null=True)
    inv_Gresineux = models.IntegerField(null=True)
    inv_GTotal = models.IntegerField(null=True)
    inv_ChampLib1Titre = models.TextField(null=True)
    inv_ChampLib2Titre = models.TextField(null=True)
    inv_ChampLib3Titre = models.TextField(null=True)
    inv_TitreFeuillus1 = models.TextField(null=True)
    inv_TitreFeuillus2 = models.TextField(null=True)
    inv_IntervTitre = models.TextField(null=True)
    inv_VoChene16P = models.IntegerField(null=True)
    inv_VoDouglas16P = models.IntegerField(null=True)
    inv_VoEpicea16P = models.IntegerField(null=True)
    inv_VoErable16P = models.IntegerField(null=True)
    inv_VoFeu16P = models.IntegerField(null=True)
    inv_VoFeuillus116P = models.IntegerField(null=True)
    inv_VoFeuillus216P = models.IntegerField(null=True)
    inv_VoFeuDiv16P = models.IntegerField(null=True)
    inv_VoFrene16P = models.IntegerField(null=True)
    inv_VoHetre16P = models.IntegerField(null=True)
    inv_VoMeleze16P = models.IntegerField(null=True)
    inv_VoMerisier16P = models.IntegerField(null=True)
    inv_VoPeuplier16P = models.IntegerField(null=True)
    inv_VoPin16P = models.IntegerField(null=True)
    inv_VoRes16P = models.IntegerField(null=True)
    inv_VoResDiv16P = models.IntegerField(null=True)
    inv_VoSapin16P = models.IntegerField(null=True)
    inv_Vo16P = models.IntegerField(null=True)
    inv_Vo1016 = models.IntegerField(null=True)
    inv_VoChene1016 = models.IntegerField(null=True)
    inv_VoDouglas1016 = models.IntegerField(null=True)
    inv_VoEpicea1016 = models.IntegerField(null=True)
    inv_VoErable1016 = models.IntegerField(null=True)
    inv_VoFeuillus11016 = models.IntegerField(null=True)
    inv_VoFeuillus21016 = models.IntegerField(null=True)
    inv_VoFeu1016 = models.IntegerField(null=True)
    inv_VoFeuDiv1016 = models.IntegerField(null=True)
    inv_VoFrene1016 = models.IntegerField(null=True)
    inv_VoHetre1016 = models.IntegerField(null=True)
    inv_VoMeleze1016 = models.IntegerField(null=True)
    inv_VoMerisier1016 = models.IntegerField(null=True)
    inv_VoPeuplier1016 = models.IntegerField(null=True)
    inv_VoPin1016 = models.IntegerField(null=True)
    inv_VoRes1016 = models.IntegerField(null=True)
    inv_VoResDiv1016 = models.IntegerField(null=True)
    inv_VoSapin1016 = models.IntegerField(null=True)
